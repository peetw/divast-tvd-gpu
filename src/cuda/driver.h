#ifndef _DRIVER_H_
#define _DRIVER_H_

// Determine active cells
void driverActiveCells( const dim3, const dim3, const float, const bool*, const float*, const float*, bool* );


// Determine active cells and calculate timestep
void driverActiveCellsTimeStep(	const dim3, const dim3, const unsigned int , const unsigned int,
								const float, const float, const float, const float, const float,
							 	const bool*, const float*, const float*, const float*, const float*,
							 	bool*, float*, float*, float& );


// Return the maximum value in float array
float driverReductionMax(	const unsigned int, const unsigned int, const unsigned int, float*, float* );


// Carry out wetting check
void driverWetting( const dim3, const dim3, const float, const bool*, const bool*, float* );


// HYDBND
void driverHydbnd(	const int, const int, const int, const int, const int, const int,
					const int, const int, const int, const int, const int,
					const int, const int, const int, const int, const int,
					const int, const int, const int, const int, const float,
					const float, const int*, const float*, const float*, const float*,
					const float*, const float*, const float*, const float*, const float*,
					const float*, const float*, const float*, const float*, const float*,
					const float*, const float*, const float*, const float*, const float*,
					const float*, float*, float*, float*, float* );

// HYDMOD_X
void driverHYDMODX(	const dim3, const dim3, const int, const int, const float, const float, const float,
					const float, const float, const float, const float, const float, const float,
					const bool*, const bool*, const float*, const float*, const float*,
					bool*, float*, float*, float*, float*, float*, float*, float*, float*, float*,
					float*, float*, float*, float* );

// TVD_X
void driverTVDX(	const dim3, const dim3, const bool, const float, const float, const bool*, const bool*,
					const float*, const float*, const float*, const float*,
					float*, float*, float*, float*, float*, float*, float*, float* );


// HYDMOD_Y
void driverHYDMODY(	const dim3, const dim3, const int, const int, const float, const float, const float,
					const float, const float, const float, const float, const float, const float,
					const bool*, const bool*, const float*, const float*, const float*,
					bool*, float*, float*, float*, float*, float*, float*, float*, float*, float*,
					float*, float*, float*, float* );

// TVD_Y
void driverTVDY(	const dim3, const dim3, const bool, const float, const float, const bool*, const bool*,
					const float*, const float*, const float*, const float*,
					float*, float*, float*, float*, float*, float*, float*, float* );


// Calculate bed shear stress
void driverBedShearStress(	const dim3, const dim3, const int, const float,
							const float*, const float*, const float*, const float*, const float*,
							float*, float*, float*, float* );

#endif // _DRIVER_H_
