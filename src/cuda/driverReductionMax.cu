#include "Header_CU.h"

// Driver for kernel to calculate sum of array on GPU using parallel reduction
float driverReductionMax(	const unsigned int INIT_THREADS, const unsigned int INIT_BLOCKS, const unsigned int NUM_ELEMS,
							float* vals_d, float* ans_d )
{
	// Set initial kernel parameters
	dim3 dimBlock( INIT_THREADS, 1, 1 );
	dim3 dimGrid( INIT_BLOCKS, 1, 1 );

	// Allocate two warps worth of shared memory so that we don't index shared memory out of bounds
	unsigned int smemSize = (INIT_THREADS <= 32) ? 2 * INIT_THREADS * sizeof(float) : INIT_THREADS * sizeof(float);

	// Launch kernel
	kernelReductionMax<<< dimGrid, dimBlock, smemSize >>>( vals_d, ans_d, NUM_ELEMS );
	utilCheckCUDAError( "Reduction kernel" );

    // Find maximum of the partial block maxima from the initial run
    unsigned int numPartialMaxima = INIT_BLOCKS;
    unsigned int threads = 0, blocks = 0;
    const unsigned int MAX_THREADS = 256;
    const unsigned int MAX_BLOCKS = 64;

    while( numPartialMaxima > 1 )
    {
    	// Calculate block size and number of blocks
        threads = (numPartialMaxima < MAX_THREADS*2) ? nextPow2((numPartialMaxima + 1)/ 2) : MAX_THREADS;
        blocks = (numPartialMaxima + (threads * 2 - 1)) / (threads * 2);
        blocks = min(MAX_BLOCKS, blocks);

        // Set kernel parameters
        dimBlock.x = threads;
        dimGrid.x = blocks;

        // Allocate two warps worth of shared memory so that we don't index shared memory out of bounds
        smemSize = (threads <= 32) ? 2 * threads * sizeof(float) : threads * sizeof(float);

        // Luanch kernel
        kernelReductionMax<<< dimGrid, dimBlock, smemSize >>>( ans_d, ans_d, numPartialMaxima );
        utilCheckCUDAError( "Reduction kernel" );

        // Update number of partial results to sum up
        numPartialMaxima = (numPartialMaxima + (threads*2-1)) / (threads*2);
    }

    // Return complete sum
    float max;
    cudaMemcpy( &max, ans_d, sizeof(float), cudaMemcpyDeviceToHost );
    return max;
}
