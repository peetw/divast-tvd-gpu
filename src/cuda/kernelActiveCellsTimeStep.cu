__global__ void kernelActiveCellsTimeStep(	const float PRESET, const bool* IALL, const float* H, const float* etu,
											const float* qxu, const float* qyu, bool* iact, float* vel )
{
	// Element index
	unsigned int I =	blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
						threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active and calculate magnitude of velocity
	if( IALL[I] )
	{
		float DP = H[I] + etu[I];
		bool WET = (DP > PRESET);

		iact[I] = WET;
		vel[I] = (WET) ? sqrtf(9.81f*DP) + sqrtf(qxu[I]*qxu[I] + qyu[I]*qyu[I]) / DP : 0.0f;
	}
}
