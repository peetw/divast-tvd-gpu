#include "Header_CU.h"

void utilCheckCUDAError( const char* msg )
{
    cudaError_t err = cudaGetLastError();

    if( err != cudaSuccess )
    {
    	fprintf( stderr, "Cuda error:\n%s: %s.\n\n", msg, cudaGetErrorString( err ) );

        exit( EXIT_FAILURE );
    }
}
