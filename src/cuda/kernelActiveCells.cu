__global__ void kernelActiveCells(	const float PRESET, const bool* IALL, const float* H, const float* etu, bool* iact )
{
	// Element index
	unsigned int I =	blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
						threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active (ternary operator results in 1 less instruction)
	iact[I] = (H[I] + etu[I] > PRESET) * IALL[I];


	// Original code:
//	if( IALL[I] )
//	{
//		if( H[I] + etu[I] > PRESET )
//		{
//			iact[I] = true;
//		}
//		else
//		{
//			iact[I] = false;
//		}
//	}
}
