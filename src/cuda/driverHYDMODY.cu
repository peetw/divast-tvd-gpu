#include "Header_CU.h"

void driverHYDMODY(	const dim3 GRID, const dim3 BLOCK, const int NFLCHZ, const int NFLEDV, const float PRESET, const float DPMIN,
					const float PRESES, const float SSLOPE, const float dt, const float DX,
					const float ANGLAT, const float BETA, const float EDCOEF, const bool* IWET_d, const bool* IALL_d,
					const float* H_d, const float* VARCHZ_d, const float* qm_d, bool* iact_d,
					float* etl_d, float* qxl_d, float* qyl_d, float* chz_d, float* eddy_d, float* fx_d, float* fy_d,
					float* ettmp_d, float* qxtmp_d, float* qytmp_d,
					float* etu_d, float* qxu_d, float* qyu_d )
{
	// Calculate constants
	const float DTDX = dt / DX;
	const float DTDXSQ = DTDX / DX;
	const float HDT = 0.5 * dt;
	const float HDTDXG = 0.5 * DTDX * 9.81;
	const float DTDXBETA = DTDX * BETA;
	const float DTG = dt * 9.81;
	const float DTCORI = dt * 3.1415926 * sin( ANGLAT * (3.1415926 / 180.0) ) / 21600.0;
	const float DET = SSLOPE * DX;


	//==========================================================//
	//															//
	//				HYDMODY_Predictor_Drying					//
	//															//
	//==========================================================//

	// Launch kernel
	HYDMODY_Predictor_Drying<<< GRID, BLOCK >>>( PRESET, IALL_d, H_d, etu_d, qxu_d, qyu_d, iact_d, etl_d, qxl_d, qyl_d );
	utilCheckCUDAError( "HYDMODY_Predictor Drying kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Predictor_Interface					//
	//															//
	//==========================================================//

	// Launch kernel
	HYDMODY_Predictor_Interface<<< GRID, BLOCK >>>(	IWET_d, iact_d, qyl_d );
	utilCheckCUDAError( "HYDMODY_Predictor Inter kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Predictor_Chezy						//
	//															//
	//==========================================================//

	// Only calculate CHZ if NFLCHZ != 1
	if( NFLCHZ != 1 )
	{
		// Launch kernel
		kernelChezy<<< GRID, BLOCK >>>( NFLCHZ, PRESET, VARCHZ_d, H_d, etl_d, qxl_d, qyl_d, chz_d );
		utilCheckCUDAError( "HYDMODY_Predictor Chezy kernel" );
	}


	//==========================================================//
	//															//
	//				HYDMODY_Predictor_Eddy						//
	//															//
	//==========================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, VARCHZ_d, eddy_d );
		utilCheckCUDAError( "HYDMODY_Predictor Eddy kernel" );
	}
	else
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, chz_d, eddy_d );
		utilCheckCUDAError( "HYDMODY_Predictor Eddy kernel" );
	}



	//==========================================================//
	//															//
	//				HYDMODY_Predictor_GUGV						//
	//															//
	//==========================================================//

	// Launch kernel
	kernelGUGV<<< GRID, BLOCK >>>( iact_d, H_d, etl_d, qxl_d, qyl_d, fx_d, fy_d );
	utilCheckCUDAError( "HYDMODY_Predictor GUGV kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Predictor							//
	//															//
	//==========================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		HYDMODY_Predictor<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI,
												DET, IWET_d, iact_d, H_d, VARCHZ_d, eddy_d, fx_d, fy_d, qm_d,
												etl_d, qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d );
		utilCheckCUDAError( "HYDMODY_Predictor kernel" );
	}
	else
	{
		HYDMODY_Predictor<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI,
												DET, IWET_d, iact_d, H_d, chz_d, eddy_d, fx_d, fy_d, qm_d,
												etl_d, qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d );
		utilCheckCUDAError( "HYDMODY_Predictor kernel" );
	}


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_Drying					//
	//															//
	//==========================================================//

	// Launch kernel
	HYDMODY_Corrector_Drying<<< GRID, BLOCK >>>( DPMIN, IWET_d, IALL_d, H_d, etl_d, qxl_d, qyl_d, iact_d, ettmp_d, qxtmp_d, qytmp_d );
	utilCheckCUDAError( "HYDMODY_Corrector Drying kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_Interface					//
	//															//
	//==========================================================//

	// Launch kernel
	HYDMODY_Corrector_Interface<<< GRID, BLOCK >>>(	IWET_d, iact_d, qytmp_d );
	utilCheckCUDAError( "HYDMODY_Corrector Inter kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_Chezy						//
	//															//
	//==========================================================//

	// Only calculate CHZ if NFLCHZ != 1
	if( NFLCHZ != 1 )
	{
		// Launch kernel
		kernelChezy<<< GRID, BLOCK >>>( NFLCHZ, PRESET, VARCHZ_d, H_d, ettmp_d, qxtmp_d, qytmp_d, chz_d );
		utilCheckCUDAError( "HYDMODY_Corrector Chezy kernel" );
	}


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_Eddy						//
	//															//
	//==========================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, VARCHZ_d, eddy_d );
		utilCheckCUDAError( "HYDMODY_Corrector Eddy kernel" );
	}
	else
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, chz_d, eddy_d );
		utilCheckCUDAError( "HYDMODY_Corrector Eddy kernel" );
	}


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_GUGV						//
	//															//
	//==========================================================//

	// Launch kernel
	kernelGUGV<<< GRID, BLOCK >>>( iact_d, H_d, ettmp_d, qxtmp_d, qytmp_d, fx_d, fy_d );
	utilCheckCUDAError( "HYDMODY_Corrector GUGV kernel" );


	//==========================================================//
	//															//
	//				HYDMODY_Corrector_Hydmod					//
	//															//
	//==========================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		HYDMODY_Corrector<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, DET,
												IWET_d, iact_d, H_d, VARCHZ_d, eddy_d, fx_d, fy_d, qm_d, etl_d,
												qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );
		utilCheckCUDAError( "HYDMODY_Corrector kernel" );
	}
	else
	{
		HYDMODY_Corrector<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, DET,
												IWET_d, iact_d, H_d, chz_d, eddy_d, fx_d, fy_d, qm_d, etl_d,
												qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );
		utilCheckCUDAError( "HYDMODY_Corrector kernel" );
	}
}
