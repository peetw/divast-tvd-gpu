__global__ void kernelWetting( const float PRESET, const bool* IWET, const bool* iact, float* etu )
{
	// Element index
	unsigned int I =	blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
						threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Offset for J index
	unsigned int offset = gridDim.y*blockDim.y;

	// Ignore the first and last columns and rows
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[I] && !iact[I] )
	{
		float ttmp = etu[I];
		unsigned int indx;

		// Search for the neighbouring wet cell with highest water level
		float etutmp = etu[I+1];
		if( iact[I+1] && (etutmp > ttmp) )
		{
			ttmp = etutmp;
			indx = I+1;
		}

		etutmp = etu[I-1];
		if( iact[I-1] && (etutmp > ttmp) )
		{
			ttmp = etutmp;
			indx = I-1;
		}

		etutmp = etu[I+offset];
		if( iact[I+offset] && (etutmp > ttmp) )
		{
			ttmp = etutmp;
			indx = I+offset;
		}

		etutmp = etu[I-offset];
		if( iact[I-offset] && (etutmp > ttmp) )
		{
			ttmp = etutmp;
			indx = I-offset;
		}

		// Increase water level in current cell and decrease in neighbour
		etutmp = etu[I];
		if( (ttmp - etutmp) > (PRESET * 2.0f) )
		{
			etu[I] = etutmp + PRESET;
			etu[indx] = etu[indx] - PRESET;
		}
	}
}
