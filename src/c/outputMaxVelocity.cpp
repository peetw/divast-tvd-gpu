#include "Header_CPP.h"

void outputMaxVelocity( ostream &fileDomain, const int IMAX, const int JMAX, const float DX, const bool* IWET, const float* uvmax )
{
	// Ouput max water elevation for whole domain over the course of the simulation
	fileDomain.precision(4);
	fileDomain	<< " Max velocity\n"
				<< "IMAX:\t" << IMAX << "\tJMAX:\t" << JMAX << "\n"
				<< "X\t\t\t\tY\t\t\t\tUVMAX" << "\n";

	for( unsigned int j = 0; j < JMAX; j++ )
	{
		for( unsigned int i = 0; i < IMAX; i++ )
		{
			unsigned int indx = i + j*IMAX;

			fileDomain	<< std::scientific << std::uppercase << i*DX << "\t" << j*DX << "\t" << uvmax[indx] << "\t" << IWET[indx] << "\n";
		}
	}
}
