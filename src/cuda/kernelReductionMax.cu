// Calculate the maximum of each block of the input array
__global__ void kernelReductionMax( float* inData, float* outData, const unsigned int NUM_ELEMS )
{
	// Initialize shared memory
	extern __shared__ float sdata[];

	// Thread indexes
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockDim.x*2 + threadIdx.x;
    unsigned int gridSize = blockDim.x*2*gridDim.x;

    // Initialize partial maximum to negative inf
    float myMax = -1E37f;

    // Reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while( i < NUM_ELEMS )
    {
        myMax = (myMax < inData[i]) ? inData[i] : myMax;
        // Ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if( i + blockDim.x < NUM_ELEMS )
            myMax = (myMax < inData[i+blockDim.x]) ? inData[i+blockDim.x] : myMax;
        i += gridSize;
    }

    // Each thread puts its local maximum into shared memory
    sdata[tid] = myMax;
    __syncthreads();

    // Do reduction in shared mem
    if (blockDim.x >= 512) { if (tid < 256) { sdata[tid] = myMax = (myMax < sdata[tid + 256]) ? sdata[tid + 256] : myMax; } __syncthreads(); }
    if (blockDim.x >= 256) { if (tid < 128) { sdata[tid] = myMax = (myMax < sdata[tid + 128]) ? sdata[tid + 128] : myMax; } __syncthreads(); }
    if (blockDim.x >= 128) { if (tid <  64) { sdata[tid] = myMax = (myMax < sdata[tid +  64]) ? sdata[tid +  64] : myMax; } __syncthreads(); }

    if (tid < 32)
    {
        // now that we are using warp-synchronous programming (below)
        // we need to declare our shared memory volatile so that the compiler
        // doesn't reorder stores to it and induce incorrect behavior.
        volatile float* smem = sdata;
        if (blockDim.x >=  64) { smem[tid] = myMax = (myMax < smem[tid + 32]) ? smem[tid + 32] : myMax; }
        if (blockDim.x >=  32) { smem[tid] = myMax = (myMax < smem[tid + 16]) ? smem[tid + 16] : myMax; }
        if (blockDim.x >=  16) { smem[tid] = myMax = (myMax < smem[tid +  8]) ? smem[tid +  8] : myMax; }
        if (blockDim.x >=   8) { smem[tid] = myMax = (myMax < smem[tid +  4]) ? smem[tid +  4] : myMax; }
        if (blockDim.x >=   4) { smem[tid] = myMax = (myMax < smem[tid +  2]) ? smem[tid +  2] : myMax; }
        if (blockDim.x >=   2) { smem[tid] = myMax = (myMax < smem[tid +  1]) ? smem[tid +  1] : myMax; }
    }

    // Write result for this block to global mem
    if (tid == 0)
        outData[blockIdx.x] = sdata[0];
}
