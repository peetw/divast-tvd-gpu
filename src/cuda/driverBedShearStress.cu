#include "Header_CU.h"

// Calculate bed shear stresses in x and y direction and update max shear stress magnitude
void driverBedShearStress(	const dim3 GRID, const dim3 BLOCK, const int NFLCHZ, const float PRESET, const float* VARCHZ_d,
							const float* H_d, const float* etu_d, const float* qxu_d, const float* qyu_d,
							float* chz_d, float* tbx_d, float* tby_d, float* taumax_d )
{
	// Set constants
	const float RHO = 1000.0f;
	const float GRAV = 9.81f;

	// Only calculate CHZ if NFLCHZ != 1
	if( NFLCHZ != 1 )
	{
		// Launch kernel
		kernelChezy<<< GRID, BLOCK >>>( NFLCHZ, PRESET, VARCHZ_d, H_d, etu_d, qxu_d, qyu_d, chz_d );
		utilCheckCUDAError( "BedShearStress Chezy kernel" );
	}

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		kernelBedShearStress<<< GRID, BLOCK >>>( RHO, GRAV, PRESET, VARCHZ_d, H_d, etu_d, qxu_d, qyu_d, tbx_d, tby_d, taumax_d );
		utilCheckCUDAError( "BedShearStress Main kernel" );
	}
	else
	{
		kernelBedShearStress<<< GRID, BLOCK >>>( RHO, GRAV, PRESET, chz_d, H_d, etu_d, qxu_d, qyu_d, tbx_d, tby_d, taumax_d );
		utilCheckCUDAError( "BedShearStress Main kernel" );
	}
}
