#include "../fortran/Header_F.h"
#include "../c/Header_CPP.h"
#include "Header_CU.h"

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	//

				DIVAST-TVD DEVELOPED BY:

				DR. DONGFANG LIANG, PROF. R.A. FALCONER, DR. BINLIANG LIN
				SCHOOL OF ENGINEERING
				CARDIFF UNIVERSITY
				CARDIFF, CF24 3TB
				UNITED KINGDOM

				MATHEMATICAL MODEL "DIVAST-TVD": - (C) COPYRIGHT 2005-2006
				(DIVAST - DEPTH INTEGRATED VELOCITIES AND SOLUTE TRANSPORT)
				(TVD - TOTAL VARIATION DIMINISHING)


				GPU ADAPTATION DEVELOPED BY:

				PETER WHITTAKER (RESEARCH STUDENT)
				SCHOOL OF ENGINEERING
				CARDIFF UNIVERSITY
				CARDIFF, CF24 3TB
				UNITED KINGDOM

				LATEST VER.: JUNE 2011

				MODEL REQUIRES: ( 50 + (IMAX*JMAX*63)/1024^2 ) MB OF AVAILABLE GRAPHICS CARD RAM

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	*/

/*
 * TO DO:
 *
 * Investigate faster versions of sqrt, i.e. _sqrt, on the device - DONE (compiler option)
 *
 * Add turbulence closure - PARTIAL (added constant eddy viscosity, still need to add k-e)
 *
 *
 *
 */

// Main program
int main( int argc, char *argv[] )
{
	//	=================================== Command line parameters =====================================	//
	string FILE_NAME;

	if( argc == 2 )
	{
		FILE_NAME = argv[1];
	}
	else
	{
		printf( "\nINCORRECT SYNTAX\n\nUsage:\n%s <FILE_NAME>\n\n", argv[0] );
		exit(0);
	}

	// Model name
	string modelName = FILE_NAME;
	size_t found = modelName.rfind(".dat");
	if( found != string::npos )
		modelName.replace( found, 4, "" );


	//	====================================== Timing log file ==========================================	//

	string filenameTiming = "out/" + modelName + "_TM.dat";
	std::ofstream fileTiming;
	fileTiming.open( filenameTiming.c_str() );

	time_t startDate;
	time(&startDate);

	fileTiming	<< "*******************************************************************\n"
				<< " START DATE AND TIME: " << ctime(&startDate)
				<< "*******************************************************************\n";

	//	=================================== Initialize GPU device =======================================	//

	// Choose device and create context
	cuInit(0);
	CUdevice dev;
	cuDeviceGet(&dev, 0);
	CUcontext ctx;
	cuCtxCreate(&ctx, 0, dev);


	//	=================================== Filenames and locations =====================================	//

	// Max filename length
	const size_t STR_LEN = 256;

	// Input file names
	string filenameParams;
	string filenameTSeries;
	string filenameBC;
	string filenameMPoints;
	string filenameDomain;
	string filenameIVals;

	// Read in file names
	readFilenames(	FILE_NAME, filenameParams, filenameTSeries, filenameBC, filenameMPoints, filenameDomain,
					filenameIVals );


	//	============================ Control parameters and array lengths ==============================	//

	// Initialize variables
	int	IMAX, JMAX, BLOCK_X, BLOCK_Y, NFLALT, NFLCHZ, NFLEDV, NFLTVD, NDATASET, NUMDATAM,
		NET1, NET2, NET3, NET4, NET8, NET9, NQX1, NQX2, NQX8, NQX9, NQY1, NQY2, NQY8, NQY9, NQM9, NCUL, NMONIT;

	float ANGLAT, BETA, CRANT, DPMIN, DTMAX, DTMIN, DX, EDCOEF, PRESES, PRESET, SSLOPE, TIMESM, TMONIT, TPRINT;

	// Read in values
	read_params_(	filenameParams.c_str(), &STR_LEN, &IMAX, &JMAX, &BLOCK_X, &BLOCK_Y, &NFLALT, &NFLCHZ, &NFLEDV, &NFLTVD,
					&NDATASET, &NUMDATAM, &NET1, &NET2, &NET3, &NET4, &NET8, &NET9, &NQX1, &NQX2, &NQX8, &NQX9,
					&NQY1, &NQY2, &NQY8, &NQY9, &NQM9, &NCUL, &NMONIT, &ANGLAT, &BETA, &CRANT, &DPMIN,
					&DTMAX, &DTMIN, &DX, &EDCOEF, &PRESES, &PRESET, &SSLOPE, &TIMESM, &TMONIT, &TPRINT);

	// Copy parameter file to output folder
	string copyParam = "cp " + filenameParams + " out/" + FILE_NAME;
	int res = system( copyParam.c_str() );

	// Calculate total number of domain output writes
	const unsigned int TOTAL_OUT = floor(TIMESM/TPRINT) + 1;

	// Define block dimensions
	dim3 BLOCK(BLOCK_X, BLOCK_Y);

	// Calculate grid dimensions (IMAX and JMAX have to be multiples of BLOCK_Y/X respectively)
	const unsigned int X = JMAX / BLOCK_X;
	const unsigned int Y = IMAX / BLOCK_Y;

	// Define grid dimensions
	dim3 GRID( X, Y );

	// Calculate array memory sizes
	const unsigned int NUM_CELLS = IMAX * JMAX;
	const size_t ARRAY_MEM_1 = NUM_CELLS * sizeof(bool);
	const size_t ARRAY_MEM_4 = NUM_CELLS * sizeof(float);


	//	======================================== Time series ===========================================	//

	// Allocate arrays for time series
	int* NUMDATA = new int[ NDATASET ];
	float* TDATA = new float[ NDATASET*NUMDATAM ];
	float* VDATA = new float[ NDATASET*NUMDATAM ];

	// Read time series data
	read_time_series_( filenameTSeries.c_str(), &STR_LEN, &NDATASET, &NUMDATAM, NUMDATA, TDATA, VDATA );

	// Initialize device array pointers
	int* NUMDATA_d;
	float* TDATA_d;
	float* VDATA_d;

	// Allocate time series arrays on device
	cudaMalloc( (void**) &NUMDATA_d, NDATASET*sizeof(int) );
	cudaMalloc( (void**) &TDATA_d,   NDATASET*NUMDATAM*sizeof(float) );
	cudaMalloc( (void**) &VDATA_d,   NDATASET*NUMDATAM*sizeof(float) );

	// Transfer data to device
	cudaMemcpy( NUMDATA_d, NUMDATA, NDATASET*sizeof(int), cudaMemcpyHostToDevice );
	cudaMemcpy( TDATA_d,   TDATA,   NDATASET*NUMDATAM*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( VDATA_d,   VDATA,   NDATASET*NUMDATAM*sizeof(float), cudaMemcpyHostToDevice );

	// Deallocate host memory
	delete[] NUMDATA;
	delete[] TDATA;
	delete[] VDATA;


	//	====================================== Boundary conditions ======================================	//

	// Allocate arrays for boundary conditions
	float* ET1 = new float[7*NET1];
	float* ET2 = new float[6*NET2];
	float* ET3 = new float[6*NET3];
	float* ET4 = new float[5*NET4];
	float* ET8 = new float[6*NET8];
	float* ET9 = new float[5*NET9];

	float* QX1 = new float[5*NQX1];
	float* QX2 = new float[6*NQX2];
	float* QX8 = new float[6*NQX8];
	float* QX9 = new float[5*NQX9];

	float* QY1 = new float[5*NQY1];
	float* QY2 = new float[6*NQY2];
	float* QY8 = new float[6*NQY8];
	float* QY9 = new float[5*NQY9];

	float* QM9 = new float[5*NQM9];
	float* CUL = new float[6*NCUL];

	// Read in boundary conditions
	read_boundary_cond_(	filenameBC.c_str(), &STR_LEN, &NET1, &NET2, &NET3, &NET4, &NET8, &NET9,
							&NQX1, &NQX2, &NQX8, &NQX9, &NQY1, &NQY2, &NQY8, &NQY9, &NQM9, &NCUL,
							ET1, ET2, ET3, ET4, ET8, ET9,
							QX1, QX2, QX8, QX9, QY1, QY2, QY8, QY9, QM9, CUL );

	// Initialize device array pointers
	float* ET1_d;
	float* ET2_d;
	float* ET3_d;
	float* ET4_d;
	float* ET8_d;
	float* ET9_d;

	float* QX1_d;
	float* QX2_d;
	float* QX8_d;
	float* QX9_d;

	float* QY1_d;
	float* QY2_d;
	float* QY8_d;
	float* QY9_d;

	float* QM9_d;
	float* CUL_d;

	// Allocate BC arrays on device
	cudaMalloc( (void**) &ET1_d, 7*NET1*sizeof(float) );
	cudaMalloc( (void**) &ET2_d, 6*NET2*sizeof(float) );
	cudaMalloc( (void**) &ET3_d, 6*NET3*sizeof(float) );
	cudaMalloc( (void**) &ET4_d, 5*NET4*sizeof(float) );
	cudaMalloc( (void**) &ET8_d, 6*NET8*sizeof(float) );
	cudaMalloc( (void**) &ET9_d, 5*NET9*sizeof(float) );

	cudaMalloc( (void**) &QX1_d, 5*NQX1*sizeof(float) );
	cudaMalloc( (void**) &QX2_d, 6*NQX2*sizeof(float) );
	cudaMalloc( (void**) &QX8_d, 6*NQX8*sizeof(float) );
	cudaMalloc( (void**) &QX9_d, 5*NQX9*sizeof(float) );

	cudaMalloc( (void**) &QY1_d, 5*NQY1*sizeof(float) );
	cudaMalloc( (void**) &QY2_d, 6*NQY2*sizeof(float) );
	cudaMalloc( (void**) &QY8_d, 6*NQY8*sizeof(float) );
	cudaMalloc( (void**) &QY9_d, 5*NQY9*sizeof(float) );

	cudaMalloc( (void**) &QM9_d, 5*NQM9*sizeof(float) );
	cudaMalloc( (void**) &CUL_d, 6*NCUL*sizeof(float) );

	// Transfer BC data to device
	cudaMemcpy( ET1_d, ET1, 7*NET1*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( ET2_d, ET2, 6*NET2*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( ET3_d, ET3, 6*NET3*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( ET4_d, ET4, 5*NET4*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( ET8_d, ET8, 6*NET8*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( ET9_d, ET9, 5*NET9*sizeof(float), cudaMemcpyHostToDevice );

	cudaMemcpy( QX1_d, QX1, 5*NQX1*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QX2_d, QX2, 6*NQX2*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QX8_d, QX8, 6*NQX8*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QX9_d, QX9, 5*NQX9*sizeof(float), cudaMemcpyHostToDevice );

	cudaMemcpy( QY1_d, QY1, 5*NQY1*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QY2_d, QY2, 6*NQY2*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QY8_d, QY8, 6*NQY8*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( QY9_d, QY9, 5*NQY9*sizeof(float), cudaMemcpyHostToDevice );

	cudaMemcpy( QM9_d, QM9, 5*NQM9*sizeof(float), cudaMemcpyHostToDevice );
	cudaMemcpy( CUL_d, CUL, 6*NCUL*sizeof(float), cudaMemcpyHostToDevice );

	// Deallocate host memory
	delete[] ET1;
	delete[] ET2;
	delete[] ET3;
	delete[] ET4;
	delete[] ET8;
	delete[] ET9;

	delete[] QX1;
	delete[] QX2;
	delete[] QX8;
	delete[] QX9;

	delete[] QY1;
	delete[] QY2;
	delete[] QY8;
	delete[] QY9;

	delete[] QM9;
	delete[] CUL;


	//	====================================== Monitoring points =======================================	//

	// Allocate host arrays
	int* IMONIT = new int[NMONIT];
	int* JMONIT = new int[NMONIT];

	// Read in monitoring points
	read_monitoring_points_( filenameMPoints.c_str(), &STR_LEN, &NMONIT, IMONIT, JMONIT );

	// Open output file for monitoring point data
	string filenameMPointsOut = "out/" + modelName + "_LST.dat";
	std::ofstream fileMonPoints;

	if( NMONIT > 0 )
	{
		fileMonPoints.open( filenameMPointsOut.c_str() );

		fileMonPoints << "Time (s)" << "\tTotal vol (m3)";

		for( unsigned int i = 1; i <= NMONIT; i++ )
			fileMonPoints << "\tH(" << i << ")\t\tE(" << i << ")\t\tQX(" << i << ")\t\tQY(" << i << ")\t\tTX(" << i << ")\t\tTY(" << i << ")\t\t";

		fileMonPoints << "\n";
	}


	//	======================================= Domain description =====================================	//

	// Allocate host arrays
	bool* IWET = new bool[NUM_CELLS];
	bool* IALL = new bool[NUM_CELLS];

	// Read in domain description and determine IALL
	read_domain_( filenameDomain.c_str(), &STR_LEN, &IMAX, &JMAX, IWET, IALL );

	// Initialize device array pointers
	bool* IWET_d;
	bool* IALL_d;
	bool* iact_d;

	// Allocate arrays on device
	cudaMalloc( (void**) &IWET_d, ARRAY_MEM_1 );
	cudaMalloc( (void**) &IALL_d, ARRAY_MEM_1 );
	cudaMalloc( (void**) &iact_d, ARRAY_MEM_1 );

	// Transfer data to device
	cudaMemcpy( IWET_d, IWET, ARRAY_MEM_1, cudaMemcpyHostToDevice );
	cudaMemcpy( IALL_d, IALL, ARRAY_MEM_1, cudaMemcpyHostToDevice );
	cudaMemset( iact_d, false, ARRAY_MEM_1 );

	// Deallocate host memory
	delete[] IALL;


	//	====================================== Initial values =======================================	//

	// Allocate host arrays
	float* H = 		new float[NUM_CELLS];
	float* VARCHZ = new float[NUM_CELLS];
	float* qm = 	new float[NUM_CELLS];
	float* etmax = 	new float[NUM_CELLS];
	float* uvmax = 	new float[NUM_CELLS];

	// Initialize page-locked host arrays
	float* etu;
	float* qxu;
	float* qyu;

	// Allocate page-locked host arrays (faster transfers to/from CPU/GPU)
	cudaHostAlloc( (void**) &etu, ARRAY_MEM_4, cudaHostAllocDefault );
	cudaHostAlloc( (void**) &qxu, ARRAY_MEM_4, cudaHostAllocDefault );
	cudaHostAlloc( (void**) &qyu, ARRAY_MEM_4, cudaHostAllocDefault );

	// Read in initial values
	read_initial_vals_( filenameIVals.c_str(), &STR_LEN, &IMAX, &JMAX, H, etu, qxu, qyu, VARCHZ, qm, etmax, uvmax );

	// Initialize device array pointers
	float* H_d;
	float* etu_d;
	float* qxu_d;
	float* qyu_d;
	float* VARCHZ_d;
	float* qm_d;
	float* etmax_d;
	float* uvmax_d;

	// Allocate device arrays
	cudaMalloc( (void**) &H_d,      ARRAY_MEM_4 );
	cudaMalloc( (void**) &etu_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &qxu_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &qyu_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &VARCHZ_d, ARRAY_MEM_4 );
	cudaMalloc( (void**) &qm_d,     ARRAY_MEM_4 );
	cudaMalloc( (void**) &etmax_d,  ARRAY_MEM_4 );
	cudaMalloc( (void**) &uvmax_d,  ARRAY_MEM_4 );

	// Transfer data to device
	cudaMemcpy( H_d,      H,      ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( etu_d,    etu,    ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( qxu_d,    qxu,    ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( qyu_d,    qyu,    ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( VARCHZ_d, VARCHZ, ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( qm_d,     qm,     ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( etmax_d,  etmax,  ARRAY_MEM_4, cudaMemcpyHostToDevice );
	cudaMemcpy( uvmax_d,  uvmax,  ARRAY_MEM_4, cudaMemcpyHostToDevice );

	// Deallocate host arrays
	delete[] VARCHZ;
	delete[] qm;


	//	===================================== Bed shear stress =====================================	//

	// Allocate host array
	float* taumax = new float[NUM_CELLS];

	// Initialize page-locked host arrays
	float* tbx;
	float* tby;

	// Allocate page-locked host arrays (faster transfers to/from CPU/GPU)
	cudaHostAlloc( (void**) &tbx, ARRAY_MEM_4, cudaHostAllocDefault );
	cudaHostAlloc( (void**) &tby, ARRAY_MEM_4, cudaHostAllocDefault );

	// Initialize device array pointers
	float* tbx_d;
	float* tby_d;
	float* taumax_d;

	// Allocate device arrays
	cudaMalloc( (void**) &tbx_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &tby_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &taumax_d, ARRAY_MEM_4 );

	// Initialize max bed shear stress array
	cudaMemset( taumax_d, 0.0f, ARRAY_MEM_4 );


	//	====================================== Device arrays =======================================	//

	// Initialize remaining device array pointers
	float* etl_d;
	float* qxl_d;
	float* qyl_d;
	float* ettmp_d;
	float* qxtmp_d;
	float* qytmp_d;
	float* chz_d;
	float* eddy_d;
	float* fx_d;
	float* fy_d;

	// Allocate device arrays
	cudaMalloc( (void**) &etl_d,   ARRAY_MEM_4 );
	cudaMalloc( (void**) &qxl_d,   ARRAY_MEM_4 );
	cudaMalloc( (void**) &qyl_d,   ARRAY_MEM_4 );
	cudaMalloc( (void**) &ettmp_d, ARRAY_MEM_4 );
	cudaMalloc( (void**) &qxtmp_d, ARRAY_MEM_4 );
	cudaMalloc( (void**) &qytmp_d, ARRAY_MEM_4 );
	cudaMalloc( (void**) &chz_d,   ARRAY_MEM_4 );
	cudaMalloc( (void**) &eddy_d,  ARRAY_MEM_4 );
	cudaMalloc( (void**) &fx_d,    ARRAY_MEM_4 );
	cudaMalloc( (void**) &fy_d,    ARRAY_MEM_4 );

	// Initialize arrays
	cudaMemset( etl_d,   0.0f, ARRAY_MEM_4 );
	cudaMemset( qxl_d,   0.0f, ARRAY_MEM_4 );
	cudaMemset( qyl_d,   0.0f, ARRAY_MEM_4 );
	cudaMemset( ettmp_d, 0.0f, ARRAY_MEM_4 );
	cudaMemset( qxtmp_d, 0.0f, ARRAY_MEM_4 );
	cudaMemset( qytmp_d, 0.0f, ARRAY_MEM_4 );
	cudaMemset( chz_d,   0.0f, ARRAY_MEM_4 );
	cudaMemset( eddy_d,  0.0f, ARRAY_MEM_4 );
	cudaMemset( fx_d,    0.0f, ARRAY_MEM_4 );
	cudaMemset( fy_d,    0.0f, ARRAY_MEM_4 );


	//	================================= Time step parameters =====================================	//

	// Inititalize pointers to velocity arrays used in DT calculations
	float* vel_d;
	float* uvtmp_d;

	// Initialize reduction kernel parameters
	unsigned int REDUCT_THREADS = 0;
	unsigned int REDUCT_BLOCKS = 0;

	// Determine whether DT is constant or not
	float dt = 0.0;
	bool dtConst = false;

	if( (DTMAX - DTMIN) < 0.00001 )
	{
		dt = DTMIN;
		dtConst = true;
	}
	else
	{
		// Calculate dimensions for reduction kernel
		calcReductionDims( NUM_CELLS, REDUCT_THREADS, REDUCT_BLOCKS );

		// Allocate velocity arrays on device
		cudaMalloc( (void**) &vel_d,   ARRAY_MEM_4 );
		cudaMalloc( (void**) &uvtmp_d, REDUCT_BLOCKS*sizeof(float) );

		// Initialize velocity array
		cudaMemset( vel_d, 0, ARRAY_MEM_4 );
	}

	//	==================================== Control Params ========================================	//

	// Check if there is enough free memory on the device (~50MB has to be kept free)
	size_t free, total;
	cuMemGetInfo(&free, &total);
	if( free - 50*1024*1024 <= 0)
	{
		printf("\n\n***DEVICE ERROR: INSUFFICIENT FREE MEMORY. PROGRAM TERMINATING***\n\n");
		exit(0);
	}

	// Initialize loop control variables
	unsigned int numStep = 0;
	unsigned int numOut = 0;
	float timeSim = 0.0;
	float timeMon = 0.0;
	float timeOut = 0.0;
	bool alter = false;
	bool transfer = false;
	bool barrierCheck = true;
	bool cpuThread1Exit = false;
	bool cpuThread2Exit = false;


	//	========================================= Main loop =========================================	//

	// Open output file for domain
	string filenameDomainOut = "out/" + modelName + "_UVE.dat";
	std::ofstream fileDomain;
	fileDomain.open( filenameDomainOut.c_str() );


	// Begin modelling
	printf("\n\n MODELLING STARTED ...\n");

	// Launch CPU threads
	unsigned int cpuThreadId;
	omp_set_num_threads(3);
	#pragma omp parallel private( cpuThreadId )
	{
		cpuThreadId = omp_get_thread_num();

		// Start loop
		while ( timeSim < TIMESM )
		{
			//	============================ Pre-hydrodynamic calculations ==================================	//

			// Master thread does main hydrodynamic calculations on GPU while other threads handle writing output
			#pragma omp master
			{
				if( dtConst )
				{
					// Determine active cells
					driverActiveCells( GRID, BLOCK, PRESET, IALL_d, H_d, etu_d, iact_d );
				}
				else
				{
					// Determine active cells and calculate timestep
					driverActiveCellsTimeStep(	GRID, BLOCK, REDUCT_THREADS, REDUCT_BLOCKS, DTMIN, DTMAX, CRANT, DX, PRESET,
							 	 	 	 	 	IALL_d, H_d, etu_d, qxu_d, qyu_d, iact_d, vel_d, uvtmp_d, dt );
				}

				// Carry out wetting check
				driverWetting( GRID, BLOCK, PRESET, IWET_d, iact_d, etu_d );

				// Update current time and step number
				timeSim += dt;
				numStep++;

				// Alternate order of X and Y
				alter = !alter;
				if( alter )
				{
					//	================================== Y Direction ======================================	//

					// HYDBND_Y
					driverHydbnd(	IMAX, DX, NET1, NET2, NET3, NET4, NET8, NET9, NQX1, NQX2, NQX8, NQX9, NQY1,
									NQY2, NQY8, NQY9, NQM9, NCUL, NDATASET, NUMDATAM, PRESET, timeSim, NUMDATA_d,
									TDATA_d, VDATA_d, ET1_d, ET2_d, ET3_d, ET4_d, ET8_d, ET9_d, QX1_d, QX2_d,
									QX8_d, QX9_d, QY1_d, QY2_d, QY8_d, QY9_d, QM9_d, CUL_d, H_d,
									etu_d, qxu_d, qyu_d, qm_d );


					// HYDMOD_Y
					driverHYDMODY(	GRID, BLOCK, NFLCHZ, NFLEDV, PRESET, DPMIN, PRESES, SSLOPE, dt, DX, ANGLAT, BETA,
									EDCOEF, IWET_d, IALL_d, H_d, VARCHZ_d, qm_d, iact_d, etl_d, qxl_d, qyl_d, chz_d,
									eddy_d, fx_d, fy_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );


					// TVD_Y
					driverTVDY( GRID, BLOCK, alter, dt, DX, IWET_d, iact_d, H_d, etl_d, qxl_d, qyl_d,
								ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d, etmax_d, uvmax_d );


					//	================================== X Direction ======================================	//

					// HYDBND_X
					driverHydbnd(	IMAX, DX, NET1, NET2, NET3, NET4, NET8, NET9, NQX1, NQX2, NQX8, NQX9, NQY1,
									NQY2, NQY8, NQY9, NQM9, NCUL, NDATASET, NUMDATAM, PRESET, timeSim, NUMDATA_d,
									TDATA_d, VDATA_d, ET1_d, ET2_d, ET3_d, ET4_d, ET8_d, ET9_d, QX1_d, QX2_d,
									QX8_d, QX9_d, QY1_d, QY2_d, QY8_d, QY9_d, QM9_d, CUL_d, H_d,
									etu_d, qxu_d, qyu_d, qm_d );


					// HYDMOD_X
					driverHYDMODX(	GRID, BLOCK, NFLCHZ, NFLEDV, PRESET, DPMIN, PRESES, SSLOPE, dt, DX, ANGLAT, BETA,
									EDCOEF, IWET_d, IALL_d, H_d, VARCHZ_d, qm_d, iact_d, etl_d, qxl_d, qyl_d, chz_d,
									eddy_d, fx_d, fy_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );


					// TVD_X
					driverTVDX( GRID, BLOCK, alter, dt, DX, IWET_d, iact_d, H_d, etl_d, qxl_d, qyl_d,
								ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d, etmax_d, uvmax_d );
				}
				else
				{
					//	================================== X Direction ======================================	//

					// HYDBND_X
					driverHydbnd(	IMAX, DX, NET1, NET2, NET3, NET4, NET8, NET9, NQX1, NQX2, NQX8, NQX9, NQY1,
									NQY2, NQY8, NQY9, NQM9, NCUL, NDATASET, NUMDATAM, PRESET, timeSim, NUMDATA_d,
									TDATA_d, VDATA_d, ET1_d, ET2_d, ET3_d, ET4_d, ET8_d, ET9_d, QX1_d, QX2_d,
									QX8_d, QX9_d, QY1_d, QY2_d, QY8_d, QY9_d, QM9_d, CUL_d, H_d,
									etu_d, qxu_d, qyu_d, qm_d );


					// HYDMOD_X
					driverHYDMODX(	GRID, BLOCK, NFLCHZ, NFLEDV, PRESET, DPMIN, PRESES, SSLOPE, dt, DX, ANGLAT, BETA,
									EDCOEF, IWET_d, IALL_d, H_d, VARCHZ_d, qm_d, iact_d, etl_d, qxl_d, qyl_d, chz_d,
									eddy_d, fx_d, fy_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );


					// TVD_X
					driverTVDX( GRID, BLOCK, alter, dt, DX, IWET_d, iact_d, H_d, etl_d, qxl_d, qyl_d,
								ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d, etmax_d, uvmax_d );


					//	================================== Y Direction ======================================	//

					// HYDBND_Y
					driverHydbnd(	IMAX, DX, NET1, NET2, NET3, NET4, NET8, NET9, NQX1, NQX2, NQX8, NQX9, NQY1,
									NQY2, NQY8, NQY9, NQM9, NCUL, NDATASET, NUMDATAM, PRESET, timeSim, NUMDATA_d,
									TDATA_d, VDATA_d, ET1_d, ET2_d, ET3_d, ET4_d, ET8_d, ET9_d, QX1_d, QX2_d,
									QX8_d, QX9_d, QY1_d, QY2_d, QY8_d, QY9_d, QM9_d, CUL_d, H_d,
									etu_d, qxu_d, qyu_d, qm_d );


					// HYDMOD_Y
					driverHYDMODY(	GRID, BLOCK, NFLCHZ, NFLEDV, PRESET, DPMIN, PRESES, SSLOPE, dt, DX, ANGLAT, BETA,
									EDCOEF, IWET_d, IALL_d, H_d, VARCHZ_d, qm_d, iact_d, etl_d, qxl_d, qyl_d, chz_d,
									eddy_d, fx_d, fy_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );


					// TVD_Y
					driverTVDY( GRID, BLOCK, alter, dt, DX, IWET_d, iact_d, H_d, etl_d, qxl_d, qyl_d,
								ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d, etmax_d, uvmax_d );
				}

				// Calculate bed shear stress
				driverBedShearStress( GRID, BLOCK, NFLCHZ, PRESET, VARCHZ_d, H_d, etu_d, qxu_d, qyu_d, chz_d, tbx_d, tby_d, taumax_d );
			}


			//	====================================== Output data ======================================	//

			// Only the master thread can interact with the GPU
			#pragma omp master
			{
				if( timeSim > timeMon && NMONIT > 0 )
				{
					// Copy hydrodynamic data back to host
					cudaMemcpy( etu, etu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( qxu, qxu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( qyu, qyu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

					// Copy bed shear stress data back to host
					cudaMemcpy( tbx, tbx_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( tby, tby_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

					// Update transfer flag
					transfer = true;
				}

				if( (timeSim > timeOut) && !transfer )
				{
					// Copy hydrodynamic data back to host
					cudaMemcpy( etu, etu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( qxu, qxu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( qyu, qyu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

					// Copy bed shear stress data back to host
					cudaMemcpy( tbx, tbx_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
					cudaMemcpy( tby, tby_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
				}

				// Reset transfer flag
				transfer = false;
			}

			// Output threads wait for master thread to finish copying data back to the host
			if( barrierCheck )
			{
				#pragma omp barrier
			}

			// Ouput monitoring point data
			if( (cpuThreadId == 1 && timeSim > timeMon && NMONIT > 0) ||
				(cpuThreadId == 0 && timeSim > timeMon && cpuThread1Exit == true && NMONIT > 0) )
			{
				outputMonPoints(	fileMonPoints, ARRAY_MEM_1, JMAX, NMONIT, numStep, DX, TMONIT, timeSim,
									IMONIT, JMONIT, H, etu, qxu, qyu, tbx, tby, timeMon );
			}

			// Ouput whole domain data
			if( (cpuThreadId == 2 && (timeSim > timeOut)) ||
				(cpuThreadId == 0 && (timeSim > timeOut) && cpuThread2Exit == true) )
			{
				outputDomain(	fileDomain, IMAX, JMAX, DX, TOTAL_OUT, TPRINT, numStep, timeSim,
								IWET, H, etu, qxu, qyu, tbx, tby, numOut, timeOut );
			}
		}

		//	====================================== End of main loop =========================================	//

		// Update barrierCheck once output threads leave the main loop to avoid master thread hanging
		barrierCheck = false;
		if( cpuThreadId == 1 )
			cpuThread1Exit = true;
		if( cpuThreadId == 2 )
			cpuThread2Exit = true;

		// Make sure all threads have finished
		#pragma omp barrier
	}

	// Output max water elevation data
	cudaMemcpy( etmax, etmax_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
	outputMaxWaterLvl( fileDomain, IMAX, JMAX, DX, IWET, H, etmax );

	// Output max bed shear stress data
	cudaMemcpy( taumax, taumax_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
	outputMaxBedStress( fileDomain, IMAX, JMAX, DX, IWET, taumax );

	// Output max velocity data
	cudaMemcpy( uvmax, uvmax_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
	outputMaxVelocity( fileDomain, IMAX, JMAX, DX, IWET, uvmax );


	// Output finish date and time to file
	time_t endDate;
	time( &endDate );

	fileTiming	<< "\n\n\n*******************************************************************\n"
				<< " FINISH DATE AND TIME: " << ctime(&endDate)
				<< "*******************************************************************";

	// Modelling complete
	unsigned int totalSec = (unsigned int) difftime( endDate, startDate );
	unsigned int totalHour = totalSec / 3600;
	unsigned int totalMin = (totalSec - 3600*totalHour) / 60;
	totalSec = totalSec - 3600*totalHour - 60*totalMin;
	printf( "\n\n MODELLING COMPLETE - TIME TAKEN: %u (h) %u (m) %u (s)\n\n", totalHour, totalMin, totalSec );

	// Close output files
	if( NMONIT > 0 )
		fileMonPoints.close();

	fileDomain.close();
	fileTiming.close();


	//	======================================== Deallocate host memory =====================================	//

	// Deallocate monitoring point arrays
	delete[] IMONIT;
	delete[] JMONIT;

	//Deallocate domain description array
	delete[] IWET;

	// Deallocate hydrodynamic arrays
	delete[] H;
	delete[] etmax;
	delete[] uvmax;
	delete[] taumax;

	// Deallocate page-locked host arrays
	cudaFreeHost( etu );
	cudaFreeHost( qxu );
	cudaFreeHost( qyu );
	cudaFreeHost( tbx );
	cudaFreeHost( tby );


	//	======================================= Deallocate device memory ====================================	//

	// Deallocate time series arrays
	cudaFree( NUMDATA_d );
	cudaFree( TDATA_d );
	cudaFree( VDATA_d );

	// Deallocate BC arrays
	cudaFree( ET1_d );
	cudaFree( ET2_d );
	cudaFree( ET3_d );
	cudaFree( ET4_d );
	cudaFree( ET8_d );
	cudaFree( ET9_d );

	cudaFree( QX1_d );
	cudaFree( QX2_d );
	cudaFree( QX8_d );
	cudaFree( QX9_d );

	cudaFree( QY1_d );
	cudaFree( QY2_d );
	cudaFree( QY8_d );
	cudaFree( QY9_d );

	cudaFree( QM9_d );
	cudaFree( CUL_d );

	// Deallocate domain description arrays
	cudaFree( IWET_d );
	cudaFree( IALL_d );
	cudaFree( iact_d );

	// Deallocate main hydrodynamic arrays
	cudaFree( H_d );
	cudaFree( etu_d );
	cudaFree( qxu_d );
	cudaFree( qyu_d );
	cudaFree( VARCHZ_d );
	cudaFree( qm_d );
	cudaFree( etmax_d );
	cudaFree( uvmax_d );
	cudaFree( taumax_d );

	cudaFree( etl_d );
	cudaFree( qxl_d );
	cudaFree( qyl_d );

	cudaFree( tbx_d );
	cudaFree( tby_d );

	if( !dtConst )
	{
		cudaFree( vel_d );
		cudaFree( uvtmp_d );
	}

	// Deallocate temporary hydrodynamic arrays
	cudaFree( ettmp_d );
	cudaFree( qxtmp_d );
	cudaFree( qytmp_d );
	cudaFree( chz_d );
	cudaFree( eddy_d );
	cudaFree( fx_d );
	cudaFree( fy_d );


	// Clean up all runtime-related resources
	cuCtxDestroy(ctx);
	cudaDeviceReset();

	return 0;
}
