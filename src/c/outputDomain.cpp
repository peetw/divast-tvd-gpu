#include "Header_CPP.h"

void outputDomain(	ostream &fileDomain, const int IMAX, const int JMAX, const float DX, const unsigned int TOTAL_OUT,
					const float TPRINT, const unsigned int numStep, const float timeSim, const bool* IWET, const float* H, const float* etu,
					const float* qxu, const float* qyu, const float* tbx, const float* tby,
					unsigned int& numOut, float& timeOut )
{
	// Update output number and time
	numOut++;
	timeOut += TPRINT;

	// Print progress status (use %.19s format for ctime() to remove the year and \n character)
	time_t currentTime = time(NULL);
	printf("\n %.19s\t%u / %u\tStep: %-7u\tTime: %f (s)", ctime(&currentTime), numOut, TOTAL_OUT, numStep, timeSim);

	// Seperate time into minutes and seconds
//	float tMin = floor(timeSim / 60.0);
//	float tSec = timeSim - (tMin * 60);

	// Ouput data for whole domain
	fileDomain.precision(4);
	fileDomain	<< std::scientific << std::uppercase << " Time: " << timeSim << " sec\n"
				<< "IMAX:\t" << IMAX << "\tJMAX:\t" << JMAX << "\n"
				<< "X\t\tY\t\tH\t\tE\t\tQX\t\tQY\t\tTX\t\tTY\t\tIWET" << "\n\n";

	for( unsigned int j = 0; j < JMAX; j++ )
	{
		for( unsigned int i = 0; i < IMAX; i++ )
		{
			unsigned int indx = i + j*IMAX;

			fileDomain	<< std::scientific << i*DX << "\t" << j*DX << "\t"
						<< H[indx] << "\t" << etu[indx] << "\t"
						<< qxu[indx] << "\t" << qyu[indx] << "\t"
						<< tbx[indx] << "\t" << tby[indx] << "\t"
						<< IWET[indx] << "\n";
		}

		if( j < (JMAX-1) )
			fileDomain << "\n";
	}
}
