#!/bin/sh

# -----------------------------------------------------------------------------------------
# SHELL SCRIPT
# -----------------------------------------------------------------------------------------

mkdir -p debug/

for file in $(ls debug/ | grep dat | sort -n -t_ -k1,1); do
	echo $file

	find_nan=$(grep "nan" debug/$file);
	find_NaN=$(grep "NaN" debug/$file);
	find_NAN=$(grep "NAN" debug/$file);

	if [ "$find_nan" -o "$find_NaN" -o "$find_NAN" ]; then
		echo "\t*** NaN FOUND! ***\n";
		exit;
	else
		echo "\tOK"
	fi
done

echo "\n\nNo NAN detected\n\n"
