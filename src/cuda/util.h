#ifndef _UTIL_H_
#define _UTIL_H_

// Check CUDA call return success, otherwise exit program
void utilCheckCUDAError( const char* );


// Output arrays to file for debugging
void utilDebug(	const char*, const int, const int, const float, const size_t, const unsigned int, const float, const float, const float,
				const float*, const float*, const float*, const float*, const float*, const float*, const float*, const float*, const float*,
				float*, float*, float*, float*, float*, float*, float*, float*, float*, unsigned int& );

#endif // _UTIL_H_
