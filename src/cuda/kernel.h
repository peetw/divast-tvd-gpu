#ifndef _KERNEL_H_
#define _KERNEL_H_

// Determine active cells
__global__ void kernelActiveCells( const float, const bool*, const float*, const float*, bool* );


// Determine active cells and calculate velocity magnitude
__global__ void kernelActiveCellsTimeStep(	const float, const bool*, const float*, const float*, const float*, const float*,
											bool*, float* );


// Calculate the maximum value in float array
__global__ void kernelReductionMax( float*, float*, const unsigned int );


// Carry out wetting check
__global__ void kernelWetting( const float, const bool*, const bool*, float* );


// Hydrodynamic boundary conditions
__global__ void kernelHydbndVelocity(	const int, const int, const int, const int, const int, const int, const int, const int,
										const int,	const int, const int, const float, const int*, const float*, const float*,
										const float*, const float*, const float*, const float*, const float*, const float*,
										const float*, const float*, float*, float* );

__global__ void kernelHydbndWaterLvl(	const int, const int, const int, const int, const int, const int, const int,
										const int, const int, const float, const int*, const float*, const float*,
										const float*, const float*, const float*, const float*, const float*,
										const float*, const float*, const float*, const float*, float* );

__global__ void kernelHydbndVertical(	const int, const int, const int, const int, const int, const int, const float,
										const float, const int*, const float*, const float*, const float*, const float*,
										const float*, const float*, float* );


// Calculate chezy roughness coefficient
__global__ void kernelChezy( const int, const float, const float*, const float*, const float*, const float*, const float*, float* );


// Calculate eddy viscosity
__global__ void kernelEddy( const int, const float, const bool*, const float*, const float*, const float*, float* );


// HYDMOD_X
__global__ void HYDMODX_Predictor_Drying(	const float, const bool*, const float*, const float*, const float*, const float*,
											bool*, float*, float*, float* );

__global__ void HYDMODX_Predictor_Interface( const bool*, const bool*, float* );

__global__ void HYDMODX_Predictor(	const float, const float, const float, const float, const float, const float, const float, const float,
									const float, const bool*, const bool*, const float*, const float*, const float*, const float*, const float*,
									const float*, const float*, const float*, const float*, float*, float*, float* );

__global__ void HYDMODX_Corrector_Drying( 	const float, const bool*, const bool*, const float*, const float*, const float*, const float*,
											bool*, float*, float*, float* );

__global__ void HYDMODX_Corrector_Interface( const bool*, const bool*, float* );

__global__ void HYDMODX_Corrector(	const float, const float, const float, const float, const float, const float, const float, const float,
									const float, const bool*, const bool*, const float*, const float*, const float*, const float*, const float*,
									const float*, const float*, const float*, const float*, const float*, const float*, const float*,
									float*, float*, float* );

__global__ void kernelFUFV(	const bool*, const float*, const float*, const float*, const float*, float*, float* );

// TVD_X
__global__ void kernelTVDX( const float, const bool*, const float*, const float*, const float*, const float*, float*, float*, float* );

__global__ void TVDX_post_kernel(	const bool, const bool*, const bool*, const float*, const float*, const float*, const float*,
									float*, float*, float*, float*, float* );


// HYDMOD_Y
__global__ void HYDMODY_Predictor_Drying(	const float, const bool*, const float*, const float*, const float*, const float*,
											bool*, float*, float*, float* );

__global__ void HYDMODY_Predictor_Interface( const bool*, const bool*, float* );

__global__ void HYDMODY_Predictor(	const float, const float, const float, const float, const float, const float, const float, const float,
									const float, const bool*, const bool*, const float*, const float*, const float*, const float*, const float*,
									const float*, const float*, const float*, const float*, float*, float*, float* );

__global__ void HYDMODY_Corrector_Drying( 	const float, const bool*, const bool*, const float*, const float*, const float*, const float*,
											bool*, float*, float*, float* );

__global__ void HYDMODY_Corrector_Interface( const bool*, const bool*, float* );

__global__ void HYDMODY_Corrector(	const float, const float, const float, const float, const float, const float, const float, const float,
									const float, const bool*, const bool*, const float*, const float*, const float*, const float*, const float*,
									const float*, const float*, const float*, const float*, const float*, const float*, const float*,
									float*, float*, float* );

__global__ void kernelGUGV(	const bool*, const float*, const float*, const float*, const float*, float*, float* );

// TVD_Y
__global__ void kernelTVDY( const float, const bool*, const float*, const float*, const float*, const float*, float*, float*, float* );

__global__ void TVDY_post_kernel(	const bool, const bool*, const bool*, const float*, const float*, const float*, const float*,
									float*, float*, float*, float*, float* );


// Calculate bed shear stress
__global__ void kernelBedShearStress(	const float, const float, const float, const float*, const float*, const float*,
										const float*, const float*, float*, float*, float* );

#endif // _KERNEL_H_
