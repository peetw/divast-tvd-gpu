#ifndef _HEADER_CPP_H_
#define _HEADER_CPP_H_

// Include directives
#include <cstdio>
#include <cmath>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>


// Select to use from std namespace
using std::ostream;
using std::string;

// =============================================== C Declarations =============================================== //

// Input functions
void readFilenames( const string, string&, string&, string&, string&, string&, string& );


// Output functions
void outputMonPoints(	ostream&, const size_t, const int, const int, const int, const float, const float,
						const float, const int*, const int*, const float*, const float*, const float*, const float*,
						const float*, const float*, float& );

void outputDomain(	ostream&, const int, const int, const float, const unsigned int, const float, const unsigned int,
					const float, const bool*, const float*, const float*, const float*, const float*, const float*, const float*,
					unsigned int&, float& );

void outputMaxWaterLvl(	ostream&, const int, const int, const float, const bool*, const float*, const float* );

void outputMaxBedStress( ostream&, const int, const int, const float, const bool*, const float* );

void outputMaxVelocity( ostream&, const int, const int, const float, const bool*, const float* );


// Utility functions
void calcReductionDims( const unsigned int, unsigned int&, unsigned int& );

double calcElapsedTime( const struct timespec*, const struct timespec* );

void outputTiming( ostream&, const string, const double );


#endif // _HEADER_CPP_H_
