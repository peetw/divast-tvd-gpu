#include "Header_CU.h"

void driverWetting( const dim3 GRID, const dim3 BLOCK, const float PRESET, const bool* IWET_d, const bool* iact_d, float* etu_d )
{
	// Launch kernel
	kernelWetting<<< GRID, BLOCK >>>( PRESET, IWET_d, iact_d, etu_d );
	utilCheckCUDAError( "Wetting kernel" );
}
