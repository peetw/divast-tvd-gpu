#include "Header_CU.h"

void driverActiveCellsTimeStep(	const dim3 GRID, const dim3 BLOCK, const unsigned int REDUCT_THREADS, const unsigned int REDUCT_BLOCKS,
								const float DTMIN, const float DTMAX, const float CRANT, const float DX, const float PRESET,
		 	 	 	 	 	 	const bool* IALL_d,	const float* H_d, const float* etu_d, const float* qxu_d, const float* qyu_d,
								bool* iact_d, float* vel_d, float* uvtmp_d, float& dt )
{
		// Launch active cells and time step kernel
		kernelActiveCellsTimeStep<<< GRID, BLOCK >>>( PRESET, IALL_d, H_d, etu_d, qxu_d, qyu_d, iact_d, vel_d );
		utilCheckCUDAError( "ActiveCellsTimeStep kernel" );

		// Determine max velocity across domain using parallel reduction
		const unsigned int NUM_ELEMS = (BLOCK.x*BLOCK.y) * (GRID.x*GRID.y);
		const float MAX_VEL = driverReductionMax( REDUCT_THREADS, REDUCT_BLOCKS, NUM_ELEMS, vel_d, uvtmp_d );

		// Calculate new time step
		dt = CRANT * DX / ( 1.0E-10 + MAX_VEL );

		if( dt < DTMIN )
			dt = DTMIN;

		if( dt > DTMAX )
			dt = DTMAX;
}
