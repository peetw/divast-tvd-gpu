#ifndef _HEADER_CU_H_
#define _HEADER_CU_H_

// Main includes
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <omp.h>
#include <cuda.h>
#include <cuda_runtime.h>


// Project includes
#include "driver.h"
#include "kernel.h"
#include "util.h"



// Calucalte next power of 2 number
inline unsigned int nextPow2( unsigned int x )
{
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}


#endif // _HEADER_CU_H_
