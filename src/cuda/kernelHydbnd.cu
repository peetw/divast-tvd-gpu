//	========================================== Declaration ==============================================	//

__device__ float vinterp( const int, const int, const int*, const float*, const float*, const float, const float );


//	========================================== Velocity BC ==============================================	//

__global__ void kernelHydbndVelocity(	const int IMAX, const int NQX1, const int NQX2, const int NQX8,
										const int NQX9, const int NQY1, const int NQY2, const int NQY8,
										const int NQY9,	const int NDATASET, const int NUMDATAM, const float time,
										const int* NUMDATA, const float* TDATA, const float* VDATA,
										const float* QX1, const float* QX2, const float* QX8, const float* QX9,
										const float* QY1, const float* QY2, const float* QY8, const float* QY9,
										float* qxu, float* qyu )
{
	// NQX1
	if( blockIdx.x == 0 && threadIdx.x < NQX1 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = QX1[tid];
		unsigned int rowmax = QX1[tid+1];

		unsigned int colmin = QX1[tid+2];
		unsigned int colmax = QX1[tid+3];

		float ttmp = QX1[tid+4];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qxu[i+j*IMAX] = ttmp;
			}
	}


	// NQX2
	if( blockIdx.x == 1 && threadIdx.x < NQX2 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = QX2[tid];
		unsigned int rowmax = QX2[tid+1];

		unsigned int colmin = QX2[tid+2];
		unsigned int colmax = QX2[tid+3];

		int itmp = QX2[tid+4];
		int jtmp = QX2[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qxu[i+j*IMAX] = qxu[(i+itmp)+(j+jtmp)*IMAX];
			}
	}



	// NQX8
	if( blockIdx.x == 2 && threadIdx.x < NQX8 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = QX8[tid];
		unsigned int rowmax = QX8[tid+1];

		unsigned int colmin = QX8[tid+2];
		unsigned int colmax = QX8[tid+3];

		int itmp = QX8[tid+4];
		int jtmp = QX8[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qxu[i+j*IMAX] = 2.0f*qxu[(i+itmp)+(j+jtmp)*IMAX] - qxu[(i+2*itmp)+(j+jtmp*2)*IMAX];
			}
	}


	// NQX9
	if( blockIdx.x == 3 && threadIdx.x < NQX9 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = QX9[tid];
		unsigned int rowmax = QX9[tid+1];

		unsigned int colmin = QX9[tid+2];
		unsigned int colmax = QX9[tid+3];

		float ttmp = vinterp( NDATASET, NUMDATAM, NUMDATA, TDATA, VDATA, QX9[tid+4], time );

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qxu[i+j*IMAX] = ttmp;
			}
	}


	// NQY1
	if( blockIdx.x == 4 && threadIdx.x < NQY1 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = QY1[tid];
		unsigned int rowmax = QY1[tid+1];

		unsigned int colmin = QY1[tid+2];
		unsigned int colmax = QY1[tid+3];

		float ttmp = QY1[tid+4];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qyu[i+j*IMAX] = ttmp;
			}
	}


	// NQY2
	if( blockIdx.x == 5 && threadIdx.x < NQY2 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = QY2[tid];
		unsigned int rowmax = QY2[tid+1];

		unsigned int colmin = QY2[tid+2];
		unsigned int colmax = QY2[tid+3];

		int itmp = QY2[tid+4];
		int jtmp = QY2[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qyu[i+j*IMAX] = qyu[(i+itmp)+(j+jtmp)*IMAX];
			}
	}


	// NQY8
	if( blockIdx.x == 6 && threadIdx.x < NQY8 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = QY8[tid];
		unsigned int rowmax = QY8[tid+1];

		unsigned int colmin = QY8[tid+2];
		unsigned int colmax = QY8[tid+3];

		int itmp = QY8[tid+4];
		int jtmp = QY8[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qyu[i+j*IMAX] = 2.0f*qyu[(i+itmp)+(j+jtmp)*IMAX] - qyu[(i+2*itmp)+(j+jtmp*2)*IMAX];
			}
	}


	// NQY9
	if( blockIdx.x == 7 && threadIdx.x < NQY9 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = QY9[tid];
		unsigned int rowmax = QY9[tid+1];

		unsigned int colmin = QY9[tid+2];
		unsigned int colmax = QY9[tid+3];

		float ttmp = vinterp( NDATASET, NUMDATAM, NUMDATA, TDATA, VDATA, QY9[tid+4], time );

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qyu[i+j*IMAX] = ttmp;
			}
	}
}


//	============================================ Water level BC =============================================	//

__global__ void kernelHydbndWaterLvl(	const int IMAX, const int NET1, const int NET2, const int NET3,
										const int NET4, const int NET8, const int NET9,
										const int NDATASET, const int NUMDATAM, const float time,
										const int* NUMDATA, const float* TDATA, const float* VDATA,
										const float* ET1, const float* ET2, const float* ET3, const float* ET4,
										const float* ET8, const float* ET9, const float* H, const float* qxu,
										const float* qyu, float* etu )
{
	// NET1
	if( blockIdx.x == 0 && threadIdx.x < NET1 )
	{
		unsigned int tid = threadIdx.x*7;

		unsigned int rowmin = ET1[tid];
		unsigned int rowmax = ET1[tid+1];

		unsigned int colmin = ET1[tid+2];
		unsigned int colmax = ET1[tid+3];

		float ttmp = ET1[tid+4] + ET1[tid+5] * sinf(6.2832f * time / ET1[tid+6]);

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				etu[i+j*IMAX] = ttmp;
			}
	}


	// NET2
	if( blockIdx.x == 1 && threadIdx.x < NET2 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = ET2[tid];
		unsigned int rowmax = ET2[tid+1];

		unsigned int colmin = ET2[tid+2];
		unsigned int colmax = ET2[tid+3];

		int itmp = ET2[tid+4];
		int jtmp = ET2[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				etu[i+j*IMAX] = etu[(i+itmp)+(j+jtmp)*IMAX];
			}
	}


	// NET3
	if( blockIdx.x == 2 && threadIdx.x < NET3 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = ET3[tid];
		unsigned int rowmax = ET3[tid+1];

		unsigned int colmin = ET3[tid+2];
		unsigned int colmax = ET3[tid+3];

		int itmp = ET3[tid+4];
		int jtmp = ET3[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				etu[i+j*IMAX] = etu[(i+itmp)+(j+jtmp)*IMAX] + H[(i+itmp)+(j+jtmp)*IMAX] - H[i+j*IMAX];
			}
	}


	// NET4
	if( blockIdx.x == 3 && threadIdx.x < NET4 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = ET4[tid];
		unsigned int rowmax = ET4[tid+1];

		unsigned int colmin = ET4[tid+2];
		unsigned int colmax = ET4[tid+3];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				float ttmp = powf(( 1.5f / sqrtf(19.62f) * sqrtf( qxu[i+j*IMAX]*qxu[i+j*IMAX] + qyu[i+j*IMAX]*qyu[i+j*IMAX] ) / ET4[tid+4] ),0.66667f);

				etu[i+j*IMAX] = ttmp - H[i+j*IMAX];
			}
	}


	// NET8
	if( blockIdx.x == 4 && threadIdx.x < NET8 )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int rowmin = ET8[tid];
		unsigned int rowmax = ET8[tid+1];

		unsigned int colmin = ET8[tid+2];
		unsigned int colmax = ET8[tid+3];

		int itmp = ET8[tid+4];
		int jtmp = ET8[tid+5];

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				etu[i+j*IMAX] = 2.0f*etu[(i+itmp)+(j+jtmp)*IMAX] - etu[(i+2*itmp)+(j+2*jtmp)*IMAX];
			}
	}


	// NET9
	if( blockIdx.x == 5 && threadIdx.x < NET9 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = ET9[tid];
		unsigned int rowmax = ET9[tid+1];

		unsigned int colmin = ET9[tid+2];
		unsigned int colmax = ET9[tid+3];

		float ttmp = vinterp( NDATASET, NUMDATAM, NUMDATA, TDATA, VDATA, ET9[tid+4], time );

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				etu[i+j*IMAX] = ttmp;
			}
	}
}


//	========================================== Vertical flow BC =============================================	//

__global__ void kernelHydbndVertical(	const int IMAX, const int NQM9, const int NCUL, const int NDATASET,
										const int NUMDATAM, const int DX, const float PRESET, const float time,
										const int* NUMDATA, const float* TDATA, const float* VDATA,
										const float* QM9, const float* CUL, const float* H, const float* etu,
										float* qm )
{
	// NQM9
	if( blockIdx.x == 0 && threadIdx.x < NQM9 )
	{
		unsigned int tid = threadIdx.x*5;

		unsigned int rowmin = QM9[tid];
		unsigned int rowmax = QM9[tid+1];

		unsigned int colmin = QM9[tid+2];
		unsigned int colmax = QM9[tid+3];

		float ttmp = vinterp( NDATASET, NUMDATAM, NUMDATA, TDATA, VDATA, QM9[tid+4], time );

		for( unsigned int i = rowmin-1; i < rowmax; i++ )
			for( unsigned int j = colmin-1; j < colmax; j++ )
			{
				qm[i+j*IMAX] = ttmp;
			}
	}


	// NCUL
	if( blockIdx.x == 1 && threadIdx.x < NCUL )
	{
		unsigned int tid = threadIdx.x*6;

		unsigned int idxA = (CUL[tid]-1) + (CUL[tid+1]-1)*IMAX;
		unsigned int idxB = (CUL[tid+2]-1) + (CUL[tid+3]-1)*IMAX;

		float ttmp = etu[idxA] - etu[idxB];

		if( (ttmp > PRESET) && (H[idxA] + etu[idxA] > PRESET) )
		{
			ttmp = CUL[tid+5] * sqrtf( 19.62f * ttmp / CUL[tid+4] ) / (DX * DX);
			qm[idxA] = -ttmp;
			qm[idxB] = ttmp;
		}
		else if( ((-ttmp) > PRESET) && (H[idxB] + etu[idxB] > PRESET) )
		{
			ttmp = CUL[tid+5] * sqrtf( 19.62f * (-ttmp) / CUL[tid+4] ) / (DX * DX);
			qm[idxA] = ttmp;
			qm[idxB] = -ttmp;
		}
		else
		{
			qm[idxA] = 0.0f;
			qm[idxB] = 0.0f;
		}
	}
}


//	============================================= Vinterp ===================================================	//

__device__ float vinterp(	const int NDATASET, const int NUMDATAM, const int* NUMDATA,
							const float* TDATA, const float* VDATA, const float Nf, const float time )
{
	int N = (int) Nf;
	float ans = 0.0f;

	for( unsigned int i = 0; i < NUMDATA[N-1]-1; i++ )
	{
		unsigned int itmp = i+(N-1)*NUMDATAM;
		unsigned int itmpP1 = (i+1)+(N-1)*NUMDATAM;

		if( (time > TDATA[itmp]) && (time < TDATA[itmpP1]) )
		{
			ans = VDATA[itmp] + (VDATA[itmpP1] - VDATA[itmp]) * (time - TDATA[itmp]) / (TDATA[itmpP1] - TDATA[itmp]);
			return ans;
		}
	}

	return ans; // Should only return 0.0 if failed
}
