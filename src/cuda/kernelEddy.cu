// Calculate eddy viscosity
__global__ void kernelEddy( const int NFLEDV, const float EDCOEF, const bool* iact,
							const float* qx, const float* qy, const float* chz, float* eddy )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// CONSTANT EDDY VISCOSITY
	// See Fischer 1979 p.107 (Mixing and dispersion in inland and coastal waters)
	// He recommends edcoef = 0.15 from lab, but 1.0-1.5 is more applicable to DIVAST type scenarios in real world
	if( NFLEDV == 1 && iact[I] )
	{
		eddy[I] = EDCOEF * sqrtf(9.81f) * sqrtf(qx[I]*qx[I] + qy[I]*qy[I]) / chz[I];
	}

	if( NFLEDV == 2 && iact[I] )
	{

	}
}
