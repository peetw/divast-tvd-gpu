if [ $# != "1" ]; then
	echo -e "\nERROR - INCORRECT SYNTAX\nUsage: ./plot-debug [NUM]\n\nwhere NUM = [3,4,5,6,7,8] and corresponds to plotting ETL, QXL, QYL, ETU, QXU or QYU\n\n"
	exit
fi

mkdir -p debug

for file in $(ls debug/ | grep dat | sort -n -t_ -k1,1); do

OUT=debug/${file/.dat/.png}

echo "Plotting file: $file"

gnuplot << EOF

######################################################################################################################################
# SETTINGS
######################################################################################################################################

set terminal png size 1024,1024

set output "$OUT"

set macro

set view 60,30

set xyplane 0

set xrange [*:*]
set yrange [*:*]
set zrange [*:*]

set xlabel "X"
set ylabel "Y"

# Plot selected variable
splot "debug/$file" u 1:2:$1 w lines

EOF

done
