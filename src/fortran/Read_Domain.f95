SUBROUTINE READ_DOMAIN( FILENAME, STR_LEN, IMAX, JMAX, IWET, IALL )

		IMPLICIT NONE

		! 1 char*, 3 int, 2 bool*
		CHARACTER(LEN = STR_LEN), INTENT(IN) :: FILENAME
		INTEGER, INTENT(IN) :: STR_LEN, IMAX, JMAX
		LOGICAL(1), INTENT(OUT) :: IWET(IMAX,JMAX), IALL(IMAX,JMAX)

		! LOCAL VARIABLES
		INTEGER :: I, J, IM1, IP1, JM1, JP1, iwettmp(IMAX,JMAX)

!		OPEN INPUT FILE
		OPEN(11, FILE=TRIM(ADJUSTL(FILENAME)), STATUS='OLD')


!		READ IN INNER DOMAIN SPECIFICATIONS
		WRITE(*,*) 'READING DOMAIN DESCRIPTION ...'
		READ(11,'(A)')
!		DO I = 1, IMAX
!			READ(11,'(10000I1)') (iwettmp(I,J), J = 1, JMAX) !!!
!		END DO
		DO j = 1, jmax !!! switched order of i,j
			READ(11,'(10000I1)') (iwettmp(I, J), i = 1, imax) !!!
		END DO


		! convert integer 0/1 to logical false/true !!!
		do i = 1, imax
		do j = 1, jmax
			if(iwettmp(I,J) == 0) then
				iwet(I,J) = .false.
			else
				iwet(I,J) = .true.
			end if
		end do
		end do


		! DETERMINE IALL(:,:), WHICH INCLUDES BOTH INNER DOMAIN AND BOUNDARY
		DO 200 J = 1, JMAX
		DO 200 I = 1, IMAX
			IM1 = MAX( 1, I-1 )
			IP1 = MIN( IMAX, I+1 )
			JM1 = MAX( 1, J-1 )
			JP1 = MIN( JMAX, J+1 )
			IF( IWET(I,J) .OR. IWET(IM1,J) .OR. IWET(IP1,J) .OR. IWET(I,JM1) .OR. IWET(I,JP1) )  THEN
				IALL(I,J) = .TRUE.
			ELSE
				IALL(I,J) = .FALSE.
			END IF
200		CONTINUE



END SUBROUTINE READ_DOMAIN
