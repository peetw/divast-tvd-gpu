__global__ void kernelFUFV(	const bool* iact, const float* H, const float* et, const float* qx, const float* qy,
							float* fx, float* fy )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y +
					 blockIdx.y*blockDim.y + threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Calculate fx and fy (ignore first and last columns)
	if( (blockIdx.x == 0 && threadIdx.x == 0) ||
		(blockIdx.x == gridDim.x-1 && threadIdx.x == blockDim.x-1) )
	{	}
	else if( iact[I] )
	{
		float qxtmp = qx[I];
		float u = qxtmp / (H[I] + et[I]);

		fx[I] = qxtmp * u;
		fy[I] = qy[I] * u;
	}
	else
	{
		fy[I] = 0.0f; /////
	}
}
