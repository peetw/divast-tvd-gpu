// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//										HYDMODY_Predictor_Drying												//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODY_Predictor_Drying(	const float PRESET, const bool* IALL, const float* H, const float* etu,
											const float* qxu, const float* qyu, bool* iact, float* etl, float* qxl, float* qyl )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active and reset variables
	if( IALL[J] )
	{
		float ettmp = etu[J];
		bool wet = (H[J] + ettmp > PRESET);

		iact[J] = wet;

		etl[J] = ettmp;
		qxl[J] = (wet) ? qxu[J] : 0.0f;
		qyl[J] = (wet) ? qyu[J] : 0.0f;
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//											HYDMODY_Predictor_Interface											//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //

__global__ void HYDMODY_Predictor_Interface( const bool* IWET, const bool* iact, float* qyl )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int offset = gridDim.y*blockDim.y;


	// Ignore the first and last columns
	if( (threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[J] && iact[J] && (!iact[J-offset] || !iact[J+offset]) )
	{
		qyl[J] = 0.0f;	// !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE ZERO
	}
}


// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//											HYDMODY_Predictor													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODY_Predictor(	const float PRESES, const float DTDX, const float DTDXSQ, const float HDT, const float HDTDXG,
									const float DTDXBETA, const float DTG, const float DTCORI, const float DET,
									const bool* IWET, const bool* iact, const float* H,	const float* chz,
									const float* eddy, const float* gx, const float* gy, const float* qm,
									const float* etl, const float* qxl, const float* qyl,
									float* etm, float* qxm, float* qym )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int offset = gridDim.y*blockDim.y;


	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[J] )
	{
		float qyltmp = qyl[J];
		float etltmp = etl[J];

		if( iact[J] )	// Active cells
		{
			float qxltmp = qxl[J];

			if( iact[J-offset] && iact[J+offset] )
			{
				// Advective acceleration and coriolis force - Y direction
				float qymtmp = qyltmp - DTDXBETA*(gy[J+offset] - gy[J]) - DTCORI*qxltmp;

				// Pressure gradient (water slope) - Y direction
				float ttmp = etl[J+offset];

				float dptmp = H[J] + etltmp;
				float dptmpP1 = H[J+offset] + ttmp;

				ttmp = ttmp - etltmp;

				qymtmp = ( (dptmp < PRESES || dptmpP1 < PRESES) && (fabsf(ttmp) > DET) )
						? qymtmp - HDTDXG * (dptmp + dptmpP1) * copysignf(DET,ttmp)
						: qymtmp - HDTDXG * (dptmp + dptmpP1) * ttmp;

				// Bed friction - Y direction (if ttmp < 0.3 then explicit scheme, else implicit scheme)
				ttmp = DTG * sqrtf(qxltmp*qxltmp + qyltmp*qyltmp) / (dptmp*dptmp*chz[J]*chz[J]);

				qymtmp = (ttmp < 0.3f) ? qymtmp - ttmp*qyltmp : qymtmp / (1.0f+ttmp);

				// Turbulence - Y direction
				qym[J] = qymtmp + 2*DTDXSQ * eddy[J] * (qyl[J+offset]-qyltmp);
			}
			else
			{
				qym[J] = 0.0f;
			}

			// Advective acceleration - X direction
			qxltmp = qxltmp - DTDXBETA*(gx[J+offset] - gx[J]);

			// Turbulence - X direction
			qxm[J] = qxltmp + DTDXSQ * eddy[J] * (qyl[J+offset]-qyltmp);

			// Continuity equation - Y direction
			etm[J] = etltmp - DTDX*(qyl[J+offset] - qyltmp) + HDT*qm[J];
		}
		else	// Inactive cells
		{
			etm[J] = etltmp - DTDX*(qyl[J+offset] - qyltmp) + HDT*qm[J];
			qxm[J] = qxl[J];
			qym[J] = qyltmp;
		}
	}
}


// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODY_Corrector_Drying													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODY_Corrector_Drying( 	const float DPMIN, const bool* IWET, const bool* IALL,
											const float* H, const float* etl, const float* qxl, const float* qyl,
											bool* iact, float* etm, float* qxm, float* qym )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active
	if( IALL[J] )
	{
		if( !IWET[J] )
		{
			etm[J] = etl[J];
			qxm[J] = qxl[J];
			qym[J] = qyl[J];
		}

		if( iact[J] && (H[J] + etm[J] < DPMIN) )
		{
			iact[J] = false;
			qxm[J] = 0.0f;
			qym[J] = 0.0f;
		}
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODY_Corrector_Interface													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODY_Corrector_Interface( const bool* IWET, const bool* iact, float* qym )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int offset = gridDim.y*blockDim.y;


	// Ignore the first and last columns
	if( (threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[J] && iact[J] && (!iact[J-offset] || !iact[J+offset]) )
	{
		qym[J] = 0.0f;	// !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE ZERO
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODY_Corrector															//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODY_Corrector(	const float PRESES, const float DTDX, const float DTDXSQ, const float HDT, const float HDTDXG,
									const float DTDXBETA, const float DTG, const float DTCORI, const float DET,
									const bool* IWET, const bool* iact, const float* H, const float* chz,
									const float* eddy, const float* gx, const float* gy, const float* qm,
									const float* etl, const float* qxl, const float* qyl,
									const float* etm, const float* qxm, const float* qym,
									float* etu, float* qxu, float* qyu )
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int offset = gridDim.y*blockDim.y;


	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[J] )
	{
		float qymtmp = qym[J];

		if( iact[J] )	// Active cells
		{
			float etmtmp = etm[J];
			float qxmtmp = qxm[J];
			float qyutmp;

			if( iact[J-offset] && iact[J+offset] )
			{
				// Advective acceleration and coriolis force - Y direction
				qyutmp = qyl[J] - DTDXBETA*(gy[J]-gy[J-offset]) - DTCORI*qxmtmp;

				// Pressure gradient (water slope) - Y direction
				float ttmp = etm[J-offset];

				float dptmp = H[J] + etmtmp;
				float dptmpM1 = H[J-offset] + ttmp;

				ttmp = etmtmp - ttmp;

				qyutmp = ( (dptmp < PRESES || dptmpM1 < PRESES) && (fabsf(ttmp) > DET) )
						? qyutmp - HDTDXG * (dptmp + dptmpM1) * copysignf(DET,ttmp)
						: qyutmp - HDTDXG * (dptmp + dptmpM1) * ttmp;

				// Bed friction term - Y direction (if ttmp < 0.3 then explicit scheme, else implicit scheme)
				ttmp = DTG * sqrtf(qxmtmp*qxmtmp + qymtmp*qymtmp) / (dptmp*dptmp*chz[J]*chz[J]);

				qyutmp = (ttmp < 0.3f) ? qyutmp - ttmp*qymtmp : qyutmp / (1.0f+ttmp);

				// Turbulence - Y direction
				qyutmp = qyutmp + 2*DTDXSQ * eddy[J] * (qymtmp-qym[J-offset]);
			}
			else
			{
				qyutmp = 0.0f;
			}

			// Advective acceleration - X direction
			qxmtmp = qxl[J] - DTDXBETA * (gx[J] - gx[J-offset]);

			// Turbulence - X direction
			qxmtmp = qxmtmp + DTDXSQ * eddy[J] * (qymtmp-qym[J-offset]);

			// Continuity equation - Y direction
			etmtmp = etl[J] - DTDX*(qymtmp - qym[J-offset]) + HDT*qm[J];

			// Final value = 1/2 * (predictor + corrector)
			qxu[J] = 0.5f * ( qxm[J] + qxmtmp );
			qyu[J] = 0.5f * ( qymtmp + qyutmp );
			etu[J] = 0.5f * ( etm[J] + etmtmp );
		}
		else	// Inactive cells
		{
			etu[J] = 0.5f * ( etm[J] + etl[J] - DTDX * (qymtmp - qym[J-offset]) + HDT * qm[J] );
			qyu[J] = 0.5f * ( qymtmp + qyl[J] );
			qxu[J] = 0.5f * ( qxm[J] + qxl[J] );
		}
	}
}
