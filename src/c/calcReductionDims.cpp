// Calculate dimensions for reduction kernel
void calcReductionDims( const unsigned int NUM_ELEMS, unsigned int& threads, unsigned int& blocks )
{
	// Block parameters
	const unsigned int MIN_THREADS = 32;
	const unsigned int MAX_THREADS = 256;

	// Check if number of elements is a multiple of a power of two
	for( unsigned int i = MAX_THREADS; i >= MIN_THREADS; i = i/2 )
	{
		if( NUM_ELEMS % i == 0 )
		{
			threads = i;
			blocks = NUM_ELEMS / threads;
			return;
		}
	}

	// If not, find nearest match
	if( threads == 0 )
	{
		for( unsigned int i = MAX_THREADS; i > 0; i = i/2 )
		{
			if( NUM_ELEMS > i )
			{
				threads = i;
				blocks = (NUM_ELEMS / threads) + 1;
				return;
			}
		}
	}
}
