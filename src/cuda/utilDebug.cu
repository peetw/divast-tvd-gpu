#include "Header_CU.h"

void utilDebug(	const char* msg, const int IMAX, const int JMAX, const float DX, const size_t ARRAY_MEM_4,
				const unsigned int numStep, const float debugStart, const float debugEnd, const float timeSim,
				const float* etl_d, const float* qxl_d, const float* qyl_d,
				const float* etu_d, const float* qxu_d, const float* qyu_d,
				const float* chz_d, const float* fx_d, const float* fy_d,
				float* etl, float* qxl, float* qyl, float* etu, float* qxu, float* qyu,
				float* chz, float* fx, float* fy, unsigned int& numDebug )
{
	if( timeSim > debugStart && timeSim < debugEnd )
	{
		// Update output count
		numDebug++;

		// Copy data back to host
		cudaMemcpy( etl, etl_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( qxl, qxl_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( qyl, qyl_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

		cudaMemcpy( etu, etu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( qxu, qxu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( qyu, qyu_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

		cudaMemcpy( chz, chz_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( fx, fx_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );
		cudaMemcpy( fy, fy_d, ARRAY_MEM_4, cudaMemcpyDeviceToHost );

		// Create filename
		char fileName[128];
		sprintf( fileName, "debug/%u_%s.dat", numDebug, msg );

		// Output data to file
		std::ofstream file;
		file.open( fileName );

		file << "# Debugging data for " << msg << "\tStep: " << numStep << "\tTime: " << timeSim << " (s)\n";
		file << "I\t\tJ\t\tETL\t\tQXL\t\tQYL\t\tETU\t\tQXU\t\tQYU\t\tCHZ\t\tFX\t\tFY\n";

		file.precision(4);

		for( unsigned int j = 0; j < JMAX; j++ )
		{
			for( unsigned int i = 0; i < IMAX; i++ )
			{
				unsigned int indx = i + j*IMAX;

				file	<< std::scientific << std::uppercase << i*DX << "\t" << j*DX << "\t"
						<< etl[indx] << "\t" << qxl[indx] << "\t" << qyl[indx] << "\t"
						<< etu[indx] << "\t" << qxu[indx] << "\t" << qyu[indx] << "\t"
						<< chz[indx] << "\t" << fx[indx] << "\t" << fy[indx] << "\n";
			}

			file << "\n";
		}
	}
}
