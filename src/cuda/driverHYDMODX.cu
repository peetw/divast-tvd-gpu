#include "Header_CU.h"

void driverHYDMODX(	const dim3 GRID, const dim3 BLOCK, const int NFLCHZ, const int NFLEDV, const float PRESET, const float DPMIN,
					const float PRESES, const float SSLOPE, const float dt, const float DX,
					const float ANGLAT, const float BETA, const float EDCOEF, const bool* IWET_d, const bool* IALL_d,
					const float* H_d, const float* VARCHZ_d, const float* qm_d, bool* iact_d,
					float* etl_d, float* qxl_d, float* qyl_d, float* chz_d, float* eddy_d, float* fx_d, float* fy_d,
					float* ettmp_d, float* qxtmp_d, float* qytmp_d,
					float* etu_d, float* qxu_d, float* qyu_d )
{
	// Calculate constants
	const float DTDX = dt / DX;
	const float DTDXSQ = DTDX / DX;
	const float HDT = 0.5 * dt;
	const float HDTDXG = 0.5 * DTDX * 9.81;
	const float DTDXBETA = DTDX * BETA;
	const float DTG = dt * 9.81;
	const float DTCORI = dt * 3.1415926 * sin( ANGLAT * (3.1415926 / 180.0) ) / 21600.0;
	const float DET = SSLOPE * DX;


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor_Drying							//
	//																	//
	//==================================================================//

	// Launch kernel
	HYDMODX_Predictor_Drying<<< GRID, BLOCK >>>( PRESET, IALL_d, H_d, etu_d, qxu_d, qyu_d, iact_d, etl_d, qxl_d, qyl_d );
	utilCheckCUDAError( "HYDMODXPredDrying kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor_Interface							//
	//																	//
	//==================================================================//

	// Launch kernel
	HYDMODX_Predictor_Interface<<< GRID, BLOCK >>>(	IWET_d, iact_d, qxl_d );
	utilCheckCUDAError( "HYDMODXPredInter kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor_Chezy								//
	//																	//
	//==================================================================//

	// Only calculate CHZ if NFLCHZ != 1
	if( NFLCHZ != 1 )
	{
		// Launch kernel
		kernelChezy<<< GRID, BLOCK >>>( NFLCHZ, PRESET, VARCHZ_d, H_d, etl_d, qxl_d, qyl_d, chz_d );
		utilCheckCUDAError( "HYDMODXPredChezy kernel" );
	}


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor_Eddy								//
	//																	//
	//==================================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, VARCHZ_d, eddy_d );
		utilCheckCUDAError( "HYDMODXPredEddy kernel" );
	}
	else
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxl_d, qyl_d, chz_d, eddy_d );
		utilCheckCUDAError( "HYDMODXPredEddy kernel" );
	}


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor_FUFV								//
	//																	//
	//==================================================================//

	// Launch kernel
	kernelFUFV<<< GRID, BLOCK >>>( iact_d, H_d, etl_d, qxl_d, qyl_d, fx_d, fy_d );
	utilCheckCUDAError( "HYDMODXPredFUFV kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Predictor									//
	//																	//
	//==================================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		HYDMODX_Predictor<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI,
												DET, IWET_d, iact_d, H_d, VARCHZ_d, eddy_d, fx_d, fy_d, qm_d,
												etl_d, qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d );
		utilCheckCUDAError( "HYDMODXPred kernel" );
	}
	else
	{
		HYDMODX_Predictor<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI,
												DET, IWET_d, iact_d, H_d, chz_d, eddy_d, fx_d, fy_d, qm_d,
												etl_d, qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d );
		utilCheckCUDAError( "HYDMODXPred kernel" );
	}


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector_Drying							//
	//																	//
	//==================================================================//

	// Launch kernel
	HYDMODX_Corrector_Drying<<< GRID, BLOCK >>>( DPMIN, IWET_d, IALL_d, H_d, etl_d, qxl_d, qyl_d, iact_d, ettmp_d, qxtmp_d, qytmp_d );
	utilCheckCUDAError( "HYDMODXCorrectorDrying kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector_Interface							//
	//																	//
	//==================================================================//

	// Launch kernel
	HYDMODX_Corrector_Interface<<< GRID, BLOCK >>>(	IWET_d, iact_d, qxtmp_d );
	utilCheckCUDAError( "HYDMODXCorrectorInterface kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector_Chezy								//
	//																	//
	//==================================================================//

	// Only calculate CHZ if NFLCHZ != 1
	if( NFLCHZ != 1 )
	{
		// Launch kernel
		kernelChezy<<< GRID, BLOCK >>>( NFLCHZ, PRESET, VARCHZ_d, H_d, ettmp_d, qxtmp_d, qytmp_d, chz_d );
		utilCheckCUDAError( "HYDMODXCorrectorChezy kernel" );
	}


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector_Eddy								//
	//																	//
	//==================================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxtmp_d, qytmp_d, VARCHZ_d, eddy_d );
		utilCheckCUDAError( "HYDMODXCorrectorEddy kernel" );
	}
	else
	{
		kernelEddy<<< GRID, BLOCK >>>( NFLEDV, EDCOEF, iact_d, qxtmp_d, qytmp_d, chz_d, eddy_d );
		utilCheckCUDAError( "HYDMODXCorrectorEddy kernel" );
	}


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector_FUFV								//
	//																	//
	//==================================================================//

	// Launch kernel
	kernelFUFV<<< GRID, BLOCK >>>( iact_d, H_d, ettmp_d, qxtmp_d, qytmp_d, fx_d, fy_d );
	utilCheckCUDAError( "HYDMODXCorrectorFUFV kernel" );


	//==================================================================//
	//																	//
	//				HYDMODX_Corrector									//
	//																	//
	//==================================================================//

	// Launch kernel
	if( NFLCHZ == 1 )
	{
		HYDMODX_Corrector<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, DET,
												IWET_d, iact_d, H_d, VARCHZ_d, eddy_d, fx_d, fy_d, qm_d, etl_d,
												qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );
		utilCheckCUDAError( "HYDMODXCorrector kernel" );
	}
	else
	{
		HYDMODX_Corrector<<< GRID, BLOCK >>>(	PRESES, DTDX, DTDXSQ, HDT, HDTDXG, DTDXBETA, DTG, DTCORI, DET,
												IWET_d, iact_d, H_d, chz_d, eddy_d, fx_d, fy_d, qm_d, etl_d,
												qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d );
		utilCheckCUDAError( "HYDMODXCorrector kernel" );
	}
}
