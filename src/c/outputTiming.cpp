#include "Header_CPP.h"

void outputTiming( ostream& fileTiming, const string msg, const double tsec )
{
	int total = tsec * 1000;
	int hour = total / (3600*1000);
	int min = (total - 3600*1000*hour) / (60*1000);
	int sec = (total - 3600*1000*hour - 60*1000*min) / 1000;
	int msec = total - 3600*1000*hour - 60*1000*min - 1000*sec;

	fileTiming << msg << std::right << std::setw(3) << hour << " (h) " << std::setw(2) << min << " (m) " << std::setw(2) << sec << " (s) " << std::setw(3) << msec << " (ms)";
}
