#ifndef _HEADER_F_H_
#define _HEADER_F_H_

// Fortran Declarations
extern "C"
{
	void read_params_(	const char*, const size_t*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*,
						int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*, int*,
						float*, float*, float*, float*, float*, float*, float*, float*, float*,
						float*, float*, float*, float*, float* );

	void read_time_series_( const char*, const size_t*, const int*, const int*, int*, float*, float* );

	void read_boundary_cond_(	const char*, const size_t*, int*, int*, int*, int*, int*, int*, int*, int*,
								int*, int*, int*, int*, int*, int*, int*, int*,
								float*, float*, float*, float*, float*, float*, float*, float*,
								float*, float*, float*, float*, float*, float*, float*, float*);

	void read_monitoring_points_( const char*, const size_t*, const int*, int*, int* );

	void read_domain_( const char*, const size_t*, const int*, const int*, bool*, bool* );

	void read_initial_vals_(	const char*, const size_t*, const int*, const int*, float*, float*, float*, float*,
								float*, float*, float*, float* );
}


#endif // _HEADER_F_H_
