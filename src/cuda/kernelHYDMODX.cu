// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//										HYDMODX_Predictor_Drying												//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODX_Predictor_Drying(	const float PRESET, const bool* IALL, const float* H, const float* etu,
											const float* qxu, const float* qyu, bool* iact, float* etl, float* qxl, float* qyl )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active and reset variables
	if( IALL[I] )
	{
		float ettmp = etu[I];
		bool wet = (H[I] + ettmp > PRESET);

		iact[I] = wet;

		etl[I] = ettmp;
		qxl[I] = (wet) ? qxu[I] : 0.0f;
		qyl[I] = (wet) ? qyu[I] : 0.0f;
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//											HYDMODX_Predictor_Interface											//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //

__global__ void HYDMODX_Predictor_Interface( const bool* IWET, const bool* iact, float* qxl )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Ignore the first and last rows
	if( (threadIdx.y == 0 && blockIdx.y == 0) || (threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) )
	{	}
	else if( IWET[I] && iact[I] && (!iact[I-1] || !iact[I+1]) )
	{
		qxl[I] = 0.0f;	// !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE ZERO
	}
}


// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//											HYDMODX_Predictor													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODX_Predictor(	const float PRESES, const float DTDX, const float DTDXSQ, const float HDT, const float HDTDXG,
									const float DTDXBETA, const float DTG, const float DTCORI, const float DET,
									const bool* IWET, const bool* iact, const float* H,	const float* chz,
									const float* eddy, const float* fx, const float* fy, const float* qm,
									const float* etl, const float* qxl, const float* qyl,
									float* etm, float* qxm, float* qym )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[I] )
	{
		float qxltmp = qxl[I];
		float etltmp = etl[I];

		if( iact[I] )	// Active cells
		{
			float qyltmp = qyl[I];

			if( iact[I-1] && iact[I+1] )
			{
				// Advective acceleration and coriolis force - X direction
				float qxmtmp = qxltmp - DTDXBETA*(fx[I+1] - fx[I]) + DTCORI*qyltmp;

				// Pressure gradient (water slope) - X direction
				float ttmp = etl[I+1];

				float dptmp = H[I] + etltmp;
				float dptmpP1 = H[I+1] + ttmp;

				ttmp = ttmp - etltmp;

				qxmtmp = ( (dptmp < PRESES || dptmpP1 < PRESES) && (fabsf(ttmp) > DET) )
							? qxmtmp - HDTDXG * (dptmp + dptmpP1) * copysignf(DET,ttmp)
							: qxmtmp - HDTDXG * (dptmp + dptmpP1) * ttmp;

				// Bed friction - X direction (if ttmp < 0.3 then explicit scheme, else implicit scheme)
				ttmp = DTG * sqrtf(qxltmp*qxltmp + qyltmp*qyltmp) / (dptmp*dptmp*chz[I]*chz[I]);

				qxmtmp = (ttmp < 0.3f) ? qxmtmp - ttmp*qxltmp : qxmtmp / (1.0f+ttmp);

				// Turbulence - X direction
				qxm[I] = qxmtmp + 2*DTDXSQ * eddy[I] * (qxl[I+1]-qxltmp);
			}
			else
			{
				qxm[I] = 0.0f;
			}

			// Advective acceleration - Y direction
			qyltmp = qyltmp - DTDXBETA*(fy[I+1] - fy[I]);

			// Turbulence - Y direction
			qym[I] = qyltmp + DTDXSQ * eddy[I] * (qxl[I+1]-qxltmp);

			// Continuity equation - X direction
			etm[I] = etltmp - DTDX*(qxl[I+1] - qxltmp) + HDT*qm[I];
		}
		else	// Inactive cells
		{
			etm[I] = etltmp - DTDX*(qxl[I+1] - qxltmp) + HDT*qm[I];
			qxm[I] = qxltmp;
			qym[I] = qyl[I];
		}
	}
}




// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODX_Corrector_Drying													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODX_Corrector_Drying( 	const float DPMIN, const bool* IWET, const bool* IALL,
											const float* H, const float* etl, const float* qxl, const float* qyl,
											bool* iact, float* etm, float* qxm, float* qym )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Determine if cell is active
	if( IALL[I] )
	{
		if( !IWET[I] )
		{
			etm[I] = etl[I];
			qxm[I] = qxl[I];
			qym[I] = qyl[I];
		}

		if( iact[I] && (H[I] + etm[I] < DPMIN) )
		{
			iact[I] = false;
			qxm[I] = 0.0f;
			qym[I] = 0.0f;
		}
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODX_Corrector_Interface													//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODX_Corrector_Interface( const bool* IWET, const bool* iact, float* qxm )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Ignore the first and last rows
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) )
	{	}
	else if( IWET[I] && iact[I] && (!iact[I-1] || !iact[I+1]) )
	{
		qxm[I] = 0.0f;	// !THE VELOCITY AT WET/DRY INTERFACE SHOULD BE ZERO
	}
}



// ============================================================================================================ //
// ============================================================================================================ //
//																												//
//									HYDMODX_Corrector															//
//																												//
// ============================================================================================================ //
// ============================================================================================================ //
__global__ void HYDMODX_Corrector(	const float PRESES, const float DTDX, const float DTDXSQ, const float HDT, const float HDTDXG,
									const float DTDXBETA, const float DTG, const float DTCORI, const float DET,
									const bool* IWET, const bool* iact, const float* H, const float* chz,
									const float* eddy, const float* fx, const float* fy, const float* qm,
									const float* etl, const float* qxl, const float* qyl,
									const float* etm, const float* qxm, const float* qym,
									float* etu, float* qxu, float* qyu )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( IWET[I] )
	{
		float qxmtmp = qxm[I];

		if( iact[I] )	// Active cells
		{
			float etmtmp = etm[I];
			float qymtmp = qym[I];
			float qxutmp;

			if( iact[I-1] && iact[I+1] )
			{
				// Advective acceleration and coriolis force - X direction
				qxutmp = qxl[I] - DTDXBETA*(fx[I]-fx[I-1]) + DTCORI*qymtmp;

				// Pressure gradient (water slope) - X direction
				float ttmp = etm[I-1];

				float dptmp = H[I] + etmtmp;
				float dptmpM1 = H[I-1] + ttmp;

				ttmp = etmtmp - ttmp;

				qxutmp = ( (dptmp < PRESES || dptmpM1 < PRESES) && (fabsf(ttmp) > DET) )
						? qxutmp - HDTDXG * (dptmp + dptmpM1) * copysignf(DET,ttmp)
						: qxutmp - HDTDXG * (dptmp + dptmpM1) * ttmp;

				// Bed friction term - X direction (if ttmp < 0.3 then explicit scheme, else implicit scheme)
				ttmp = DTG * sqrtf(qxmtmp*qxmtmp + qymtmp*qymtmp) / (dptmp*dptmp*chz[I]*chz[I]);

				qxutmp = (ttmp < 0.3f) ? qxutmp - ttmp*qxmtmp : qxutmp / (1.0f+ttmp);

				// Turbulence - X direction
				qxutmp = qxutmp + 2*DTDXSQ * eddy[I] * (qxmtmp-qxm[I-1]);
			}
			else
			{
				qxutmp = 0.0f;
			}

			// Advective acceleration - Y direction
			qymtmp = qyl[I] - DTDXBETA * (fy[I] - fy[I-1]);

			// Turbulence - Y direction
			qymtmp = qymtmp + DTDXSQ * eddy[I] * (qxmtmp-qxm[I-1]);

            // Continuity equation - X direction
			etmtmp = etl[I] - DTDX*(qxmtmp - qxm[I-1]) + HDT*qm[I];

            // Final value = 1/2 * (predictor + corrector)
			qxu[I] = 0.5f * ( qxmtmp + qxutmp );
			qyu[I] = 0.5f * ( qym[I] + qymtmp );
			etu[I] = 0.5f * ( etm[I] + etmtmp );
		}
		else	// Inactive cells
		{
			etu[I] = 0.5f * ( etm[I] + etl[I] - DTDX * (qxmtmp - qxm[I-1]) + HDT * qm[I] );
			qxu[I] = 0.5f * ( qxmtmp + qxl[I] );
			qyu[I] = 0.5f * ( qym[I] + qyl[I] );
		}
	}
}
