//	=====================================================================================================	//
//																											//
//												TVDX_kernel													//
//																											//
//	=====================================================================================================	//
__global__ void kernelTVDX( const float DTDX, const bool* iact, const float* H,
							const float* etl, const float* qxl, const float* qyl,
							float* etltmp, float* qxltmp, float* qyltmp )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;


	// Ignore the first row, and the first and last columns
	if( (blockIdx.y == 0 && threadIdx.y == 0) ||
		(blockIdx.x == 0 && threadIdx.x == 0) ||
		(blockIdx.x == gridDim.x-1 && threadIdx.x == blockDim.x-1) )
	{	}
	else if( iact[I] && iact[I-1] )
	{
		// Load hydrodynamic values into registers
		float etlI = etl[I];
		float qxlI = qxl[I];
		float qylI = qyl[I];
		float etlIM1 = etl[I-1];
		float qxlIM1 = qxl[I-1];
		float qylIM1 = qyl[I-1];


	    // =============
	    // Delta
	    // =============

		float detm1 = etlI - etlIM1;	// etl[I] - etl[I-1]
		float dqxm1 = qxlI - qxlIM1;	// qxl[I] - qxl[I-1]
		float dqym1 = qylI - qylIM1;	// qyl[I] - qyl[I-1]


	    // =============
	    // Gradient
	    // =============

		// Initialize variables
		float gplusM1 = 0.0f;
		float gminus = 0.0f;

		// Ignore second and last rows
		if( (blockIdx.y == 0 && threadIdx.y == 1) ||
			(blockIdx.y == gridDim.y-1 && threadIdx.y == blockDim.y-1) )
		{	}
		else
		{
			// =============
			// gplus[I-1]
			// =============
			if( iact[I-2] )
			{
				// Rplus
				float tmp = (etlIM1-etl[I-2])*detm1		// etl[I-1], etl[I-2]
						  + (qxlIM1-qxl[I-2])*dqxm1		// qxl[I-1], qxl[I-2]
						  + (qylIM1-qyl[I-2])*dqym1;	// qyl[I-1], qyl[I-2]

				float r = tmp / ( 1.0E-15f + detm1*detm1 + dqxm1*dqxm1 + dqym1*dqym1 );

				// Courant
				tmp = H[I-1] + etlIM1;	// etl[I-1]
				tmp = DTDX * ( fabsf(qxlIM1/tmp) + sqrtf(9.81f*tmp) );	// qxl[I-1]

				tmp = (tmp < 0.5f) ? 0.5f*tmp*(1.0f-tmp) : 0.125f;

//				if( tmp < 0.5f )
//					tmp = 0.5f * tmp * (1.0f - tmp);
//				else
//					tmp = 0.125f;

				gplusM1 = tmp * (1.0f - max( 0.0f, min(2.0f * r, 1.0f) ));
			}

			// =============
			// gminus[I]
			// =============
			if( iact[I+1] )
			{
				// Rminus
				float tmp =	detm1*(etl[I+1]-etlI)		// etl[I+1], etl[I]
						  + dqxm1*(qxl[I+1]-qxlI)		// qxl[I+1], qxl[I]
						  + dqym1*(qyl[I+1]-qylI);		// qyl[I+1], qyl[I]

				float r = tmp / ( 1.0E-15f + detm1*detm1 + dqxm1*dqxm1 + dqym1*dqym1);

				// Courant
				tmp = H[I] + etlI;	// etl[I]
				tmp = DTDX * ( fabsf(qxlI/tmp) + sqrtf(9.81f*tmp) );	// qxl[I]

				tmp = (tmp < 0.5f) ? 0.5f*tmp*(1.0f-tmp) : 0.125f;

//				if( tmp < 0.5f )
//					tmp = 0.5f * tmp * (1.0f - tmp);
//				else
//					tmp = 0.125f;

				gminus = tmp * (1.0f - max( 0.0f, min(2.0f * r, 1.0f) ));
			}
		}


		// =============
		// Diffusion
		// =============

		float tmp = gplusM1 + gminus;

		etltmp[I] = tmp * detm1;
		qxltmp[I] = tmp * dqxm1;
		qyltmp[I] = tmp * dqym1;
	}
	else
	{
		etltmp[I] = 0.0f;
		qxltmp[I] = 0.0f;
		qyltmp[I] = 0.0f;
	}
}




// ============================================================================================================	//
//																												//
//											TVDX_post_kernel													//
//																												//
// ============================================================================================================	//
__global__ void TVDX_post_kernel(	const bool alter, const bool* IWET, const bool* iact,
									const float* H, const float* etl, const float* qxl,	const float* qyl,
									float* etu, float* qxu, float* qyu, float* etmax, float* uvmax )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int IP1 = I + 1;

	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( iact[I] && IWET[I] )
	{
		etu[I] = etu[I] + etl[IP1] - etl[I];
		qxu[I] = qxu[I] + qxl[IP1] - qxl[I];
		qyu[I] = qyu[I] + qyl[IP1] - qyl[I];

		// Update max water level and velocity
		if( alter )
		{
			float etutmp = etu[I];
			float qxutmp = qxu[I];
			float qyutmp = qyu[I];

			etmax[I] = (etutmp > etmax[I]) ? etutmp : etmax[I];

			float vel = sqrtf(qxutmp*qxutmp + qyutmp*qyutmp) / (H[I]+etutmp);
			uvmax[I] = (vel > uvmax[I]) ? vel : uvmax[I];
		}
	}
}
