//	=====================================================================================================	//
//																											//
//												TVDY_kernel													//
//																											//
//	=====================================================================================================	//
__global__ void kernelTVDY( const float DTDX, const bool* iact, const float* H,
							const float* etl, const float* qxl, const float* qyl,
							float* etltmp, float* qxltmp, float* qyltmp )
{
	// Element index
	unsigned int J =	blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
						threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int offset = gridDim.y*blockDim.y;


	// Ignore the first and last rows, and the first column
	if( (blockIdx.y == 0 && threadIdx.y == 0) ||
		(blockIdx.y == gridDim.y-1 && threadIdx.y == blockDim.y-1) ||
		(blockIdx.x == 0 && threadIdx.x == 0) )
	{	}
	else if( iact[J] && iact[J-offset] )
	{
		// Load hydrodynamic values into registers
		float etlJ = etl[J];
		float qxlJ = qxl[J];
		float qylJ = qyl[J];
		float etlJM1 = etl[J-offset];
		float qxlJM1 = qxl[J-offset];
		float qylJM1 = qyl[J-offset];


	    // =============
	    // Delta
	    // =============

		float detm1 = etlJ - etlJM1;	// etl[J] - etl[J-1]
		float dqxm1 = qxlJ - qxlJM1;	// qxl[J] - qxl[J-1]
		float dqym1 = qylJ - qylJM1;	// qyl[J] - qyl[J-1]


	    // =============
	    // Gradient
	    // =============

		// Initialize variables
		float gplusM1 = 0.0f;
		float gminus = 0.0f;

		// Ignore second and last columns
		if( (blockIdx.x == 0 && threadIdx.x == 1) ||
			(blockIdx.x == gridDim.x-1 && threadIdx.x == blockDim.x-1) )
		{	}
		else
		{
			// =============
			// gplus[J-1]
			// =============
			if( iact[J-2*offset] )
			{
				// Rplus
				float tmp = (etlJM1-etl[J-2*offset])*detm1		// etl[J-1], etl[J-2]
						  + (qxlJM1-qxl[J-2*offset])*dqxm1		// qxl[J-1], qxl[J-2]
						  + (qylJM1-qyl[J-2*offset])*dqym1;		// qyl[J-1], qyl[J-2]

				float r = tmp / ( 1.0E-15f + detm1*detm1 + dqxm1*dqxm1 + dqym1*dqym1 );

				// Courant
				tmp = H[J-offset] + etlJM1;	// etl[J-1]
				tmp = DTDX * ( fabsf(qylJM1/tmp) + sqrtf(9.81f*tmp) );	// qyl[J-1]

				tmp = (tmp < 0.5f) ? 0.5f*tmp*(1.0f-tmp) : 0.125f;

//				if( tmp < 0.5f )
//					tmp = 0.5f * tmp * (1.0f - tmp);
//				else
//					tmp = 0.125f;

				gplusM1 = tmp * (1.0f - max( 0.0f, min(2.0f * r, 1.0f) ));
			}

			// =============
			// gminus[J]
			// =============
			if( iact[J+offset] )
			{
				// Rminus
				float tmp =	detm1*(etl[J+offset]-etlJ)		// etl[J+1], etl[J]
						  + dqxm1*(qxl[J+offset]-qxlJ)		// qxl[J+1], qxl[J]
						  + dqym1*(qyl[J+offset]-qylJ);		// qyl[J+1], qyl[J]

				float r = tmp / ( 1.0E-15f + detm1*detm1 + dqxm1*dqxm1 + dqym1*dqym1);

				// Courant
				tmp = H[J] + etlJ;	// etl[J]
				tmp = DTDX * ( fabsf(qylJ/tmp) + sqrtf(9.81f*tmp) );	// qyl[J]

				tmp = (tmp < 0.5f) ? 0.5f*tmp*(1.0f-tmp) : 0.125f;

//				if( tmp < 0.5f )
//					tmp = 0.5f * tmp * (1.0f - tmp);
//				else
//					tmp = 0.125f;

				gminus = tmp * (1.0f - max( 0.0f, min(2.0f * r, 1.0f) ));
			}
		}


		// =============
		// Diffusion
		// =============

		float tmp = gplusM1 + gminus;

		etltmp[J] = tmp * detm1;
		qxltmp[J] = tmp * dqxm1;
		qyltmp[J] = tmp * dqym1;
	}
	else
	{
		etltmp[J] = 0.0f;
		qxltmp[J] = 0.0f;
		qyltmp[J] = 0.0f;
	}
}




// ============================================================================================================	//
//																												//
//											TVDY_post_kernel													//
//																												//
// ============================================================================================================	//
__global__ void TVDY_post_kernel(	const bool alter, const bool* IWET, const bool* iact,
									const float* H, const float* etl, const float* qxl, const float* qyl,
									float* etu, float* qxu, float* qyu, float* etmax, float* uvmax )
{
	// Element index
	unsigned int J =	blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
						threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	unsigned int JP1 = J + gridDim.y*blockDim.y;

	// Ignore first and last rows and first and last columns
	if( (threadIdx.y == 0 && blockIdx.y == 0) ||
		(threadIdx.y == blockDim.y-1 && blockIdx.y == gridDim.y-1) ||
		(threadIdx.x == 0 && blockIdx.x == 0) ||
		(threadIdx.x == blockDim.x-1 && blockIdx.x == gridDim.x-1) )
	{	}
	else if( iact[J] && IWET[J] )
	{
		etu[J] = etu[J] + etl[JP1] - etl[J];
		qxu[J] = qxu[J] + qxl[JP1] - qxl[J];
		qyu[J] = qyu[J] + qyl[JP1] - qyl[J];

		// Update max water level and velocity
		if( !alter )
		{
			float etutmp = etu[J];
			float qxutmp = qxu[J];
			float qyutmp = qyu[J];

			etmax[J] = (etutmp > etmax[J]) ? etutmp : etmax[J];

			float vel = sqrtf(qxutmp*qxutmp + qyutmp*qyutmp) / (H[J]+etutmp);
			uvmax[J] = (vel > uvmax[J]) ? vel : uvmax[J];
		}
	}
}
