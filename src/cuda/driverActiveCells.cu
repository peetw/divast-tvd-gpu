#include "Header_CU.h"

void driverActiveCells( const dim3 GRID, const dim3 BLOCK, const float PRESET,
						const bool* IALL_d, const float* H_d, const float* etu_d, bool* iact_d )
{
	 // Launch kernel
	 kernelActiveCells<<< GRID, BLOCK >>>( PRESET, IALL_d, H_d, etu_d, iact_d );
	 utilCheckCUDAError( "ActiveCells kernel" );
}
