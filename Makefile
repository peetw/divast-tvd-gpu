# Main directories
BINDIR = bin
INPDIR = inp
OBJDIR = obj
OUTDIR = out
FORDIR = src/fortran
CPPDIR = src/c
CUDIR = src/cuda

# Directories to create if not already existant
MKDIR := $(BINDIR) $(INPDIR) $(OBJDIR) $(OUTDIR)

# Binary file
BIN := $(BINDIR)/divast-tvd-gpu

# FORTRAN files
F_H := $(wildcard $(FORDIR)/*.h)
F_SRC := $(wildcard $(FORDIR)/*.f95)
F_OBJ := $(patsubst %.f95,%.o,$(subst $(FORDIR),$(OBJDIR),$(F_SRC)))

# CPP files
CPP_SRC := $(wildcard $(CPPDIR)/*.cpp)
CPP_OBJ := $(patsubst %.cpp,%.o,$(subst $(CPPDIR),$(OBJDIR),$(CPP_SRC)))

# CUDA files
CU_SRC := $(wildcard $(CUDIR)/*.cu)
CU_OBJ := $(patsubst %.cu,%.o,$(subst $(CUDIR),$(OBJDIR),$(CU_SRC)))

# Dependency file
DEP = .depend

# Compilers
FC = gfortran
NVCC = /usr/local/cuda/bin/nvcc

# NOTES
# Be wary of --use-fast_math
# 	For example it implements powf(x,y) as exp2f(y*__log2f(x)) and therefore
#	can't handle raising negative numbers to any power (returns nan)
# Use -Xcompiler -fopenmp with nvcc to compile CUDA + OpenMP code
# Use --ptxas-options=-v to print detailed kernel info
# Use -fopenmp with g++ to link code containing OpenMP
# Use -lrt to be able to use clock_gettime

# Compiler options
FFLAGS = -c -O3
CXXFLAGS = -c -O3
NVCCFLAGS = -c -arch=sm_20 -O3 --use_fast_math -Xcompiler -fopenmp

# Linker options
LDFLAGS = -fopenmp
LDPATHS = -L/usr/local/cuda/lib
LDLIBS = -lcuda -lcudart -lgfortran -lrt


# NON-FILE TARGETS
.PHONY: all clean

# DEFAULT TARGET
all: $(MKDIR) $(BIN)

# CREATE DIRECTORIES
$(MKDIR):
	mkdir -p $@

# CREATE BINARY
$(BIN): $(F_OBJ) $(CPP_OBJ) $(CU_OBJ)
	@$(RM) $(BIN)
	$(CXX) $(LDFLAGS) $^ $(LDPATHS) $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# DETERMINE DEPENDICES
$(DEP): $(CPP_SRC) $(CU_SRC)
	@$(CXX) -xc++ $(CXXFLAGS) -MM $^ >> $(DEP)
	@sed -i 's|.*:|$(OBJDIR)/&|' $(DEP)

-include $(DEP)

# COMPILE - FORTRAN
$(OBJDIR)/%.o: $(FORDIR)/%.f95 $(F_H)
	$(FC) $(FFLAGS) $< -o $@

# COMPILE - C++
$(OBJDIR)/%.o: $(CPPDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

# COMPILE - CUDA
$(OBJDIR)/%.o: $(CUDIR)/%.cu
	$(NVCC) $(NVCCFLAGS) $< -o $@

# CLEAN PROJECT
clean:
	$(RM) $(BIN)
	$(RM) $(DEP)
	$(RM) $(F_OBJ)
	$(RM) $(CPP_OBJ)
	$(RM) $(CU_OBJ)
