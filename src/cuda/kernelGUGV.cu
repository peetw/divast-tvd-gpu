__global__ void kernelGUGV(	const bool* iact, const float* H, const float* et, const float* qx, const float* qy,
							float* gx, float* gy)
{
	// Element index
	unsigned int J = blockIdx.x*blockDim.x*blockDim.y*gridDim.y +
					 blockIdx.y*blockDim.y + threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Calculate gx and gy (ignore first and last rows)
	if( (blockIdx.y == 0 && threadIdx.y == 0) ||
		(blockIdx.y == gridDim.y-1 && threadIdx.y == blockDim.y-1) )
	{	}
	else if( iact[J] )
	{
		float qytmp = qy[J];
		float v = qytmp / (H[J] + et[J]);

		gx[J] = qx[J] * v;
		gy[J] = qytmp * v;
	}
	else
	{
		gx[J] = 0.0; /////
	}
}
