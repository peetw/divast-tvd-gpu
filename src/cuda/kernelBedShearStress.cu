__global__ void kernelBedShearStress(	const float RHO, const float GRAV, const float PRESET, const float* chz,
										const float* H, const float* etu, const float* qxu, const float* qyu,
										float* tbx, float* tby, float* taumax )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y + blockIdx.y*blockDim.y +
					 threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	float DP = H[I] + etu[I];

	if( DP > PRESET )
	{
		// Load velocities into registers
		float qx = qxu[I];
		float qy = qyu[I];

		// Main bed stress calculation
		float tmp = ( RHO * GRAV * sqrtf(qx*qx + qy*qy) ) / (DP*DP*chz[I]*chz[I]);

		// Bed shear stresses in cartesian directions
		tbx[I] = qx * tmp;
		tby[I] = qy * tmp;

		// Calculate magnitude
		float tau = sqrtf( qx*qx*tmp*tmp + qy*qy*tmp*tmp );

		// Update max shear stress
		taumax[I] = (tau > taumax[I]) ? tau : taumax[I];
	}
	else
	{
		tbx[I] = 0.0f;
		tby[I] = 0.0f;
	}
}
