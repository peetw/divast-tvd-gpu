#include "Header_CPP.h"

void readFilenames( const string FILE_NAME, string& filenameParams, string& filenameTSeries,
					string& filenameBC, string& filenameMPoints, string& filenameDomain, string& filenameIVals )
{
	std::ifstream fileFilenames;
	fileFilenames.open( FILE_NAME.c_str() );

	getline( fileFilenames, filenameParams );
	getline( fileFilenames, filenameTSeries );
	getline( fileFilenames, filenameBC );
	getline( fileFilenames, filenameMPoints );
	getline( fileFilenames, filenameDomain );
	getline( fileFilenames, filenameIVals );

	fileFilenames.close();
}
