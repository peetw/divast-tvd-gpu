#include "Header_CU.h"


void driverTVDX(	const dim3 GRID, const dim3 BLOCK, const bool alter, const float dt, const float DX,
					const bool* IWET_d, const bool* iact_d, const float* H_d,
					const float* etl_d, const float* qxl_d, const float* qyl_d,
					float* ettmp_d, float* qxtmp_d, float* qytmp_d,
					float* etu_d, float* qxu_d, float* qyu_d, float* etmax_d, float* uvmax_d )
{
	// Calculate constants
	const float DTDX = dt / DX;

	// Launch main kernel (delta, grad, diff)
	kernelTVDX<<< GRID, BLOCK >>>(	DTDX, iact_d, H_d, etl_d, qxl_d, qyl_d, ettmp_d, qxtmp_d, qytmp_d );
	utilCheckCUDAError( "TVDX kernel" );

	// Launch post kernel
	TVDX_post_kernel<<< GRID, BLOCK >>>( alter, IWET_d, iact_d, H_d, ettmp_d, qxtmp_d, qytmp_d, etu_d, qxu_d, qyu_d, etmax_d, uvmax_d );
	utilCheckCUDAError( "TVDX Post kernel" );
}
