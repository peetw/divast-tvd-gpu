#include "Header_CU.h"

void driverHydbnd(	const int IMAX, const int DX, const int NET1, const int NET2,
					const int NET3, const int NET4, const int NET8, const int NET9, const int NQX1, const int NQX2,
					const int NQX8, const int NQX9, const int NQY1, const int NQY2, const int NQY8, const int NQY9,
					const int NQM9, const int NCUL, const int NDATASET, const int NUMDATAM, const float PRESET,
					const float time, const int* NUMDATA, const float* TDATA, const float* VDATA, const float* ET1,
					const float* ET2, const float* ET3, const float* ET4, const float* ET8, const float* ET9,
					const float* QX1, const float* QX2, const float* QX8, const float* QX9, const float* QY1,
					const float* QY2, const float* QY8, const float* QY9, const float* QM9, const float* CUL,
					const float* H, float* etu, float* qxu, float* qyu, float* qm )
{
	 // Resize grid and block
	 dim3 GRID;
	 GRID.y = 1;
	 dim3 BLOCK(32,1);

	 // Launch velocity BC kernel
	 GRID.x = 8;
	 kernelHydbndVelocity<<< GRID, BLOCK >>>(	IMAX, NQX1, NQX2, NQX8, NQX9, NQY1, NQY2, NQY8,
												NQY9, NDATASET, NUMDATAM, time, NUMDATA, TDATA, VDATA,
												QX1, QX2, QX8, QX9, QY1, QY2, QY8, QY9, qxu, qyu);
	 utilCheckCUDAError( "HydbndVelocity kernel" );

	 // Launch water level BC kernel (dependent on velocity)
	 GRID.x = 6;
	 kernelHydbndWaterLvl<<< GRID, BLOCK >>>(	IMAX, NET1, NET2, NET3, NET4, NET8, NET9, NDATASET,
			 	 	 	 	 	 	 	 	 	NUMDATAM, time, NUMDATA, TDATA, VDATA, ET1, ET2, ET3,
			 	 	 	 	 	 	 	 	 	ET4, ET8, ET9, H, qxu, qyu, etu);
	 utilCheckCUDAError( "HydbndWaterLvl kernel" );

	 // Launch vertical velocity BC kernel (dependent on water level)
	 GRID.x = 2;
	 kernelHydbndVertical<<< GRID, BLOCK >>>(	IMAX, NQM9, NCUL, NDATASET, NUMDATAM, DX, PRESET, time,
												NUMDATA, TDATA, VDATA, QM9, CUL, H, etu, qm);
	 utilCheckCUDAError( "HydbndVertical kernel" );
}
