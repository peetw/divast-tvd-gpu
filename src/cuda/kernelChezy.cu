__global__ void kernelChezy( 	const int NFLCHZ, const float PRESET, const float* VARCHZ, const float* H,
								const float* et, const float* qx, const float* qy, float* chz )
{
	// Element index
	unsigned int I = blockIdx.x*blockDim.x*blockDim.y*gridDim.y +
					 blockIdx.y*blockDim.y + threadIdx.x*blockDim.y*gridDim.y + threadIdx.y;

	// Water depth
	float DP = H[I] + et[I];

	// Manning formula
	if( NFLCHZ == 2 && (DP > PRESET) )
	{
		chz[I] = powf(DP,0.166667f) / VARCHZ[I];
	}


	// NOTES ABOUT COLEBROOK_WHITE
	//
	// Chezy is always greater when using NFL4 vs NFL3
	//
	// DP > k/12 otherwise Chezy will be negative (i.e PRESET has to be larger than k_min/12)

	// Colebrook-White formula - Transitional flow, i.e: RE ~ 2000-4000
	if( NFLCHZ == 3 && (DP > PRESET) )
	{
		float re = 4.0f * sqrtf(qx[I]*qx[I] + qy[I]*qy[I]) / 1.30E-6f;

		re = (re < 500.0f) ? 500.0f : re;

		float ttmp = VARCHZ[I] / (DP * 12.0f);
		float chztmp = -18.0f * log10f(ttmp);

		float chzold = chztmp;

		chztmp = -18.0f * log10f(ttmp + 5.0f * chzold / (re*18.0f));

		while( fabsf( chztmp - chzold ) > 0.5f )
		{
			chzold = chztmp;
			chztmp = -18.0f * log10f(ttmp + 5.0f * chzold / (re*18.0f));
		}

		chz[I] = chztmp;
	}

	// Colebrook-White formula - Fully rough flow, i.e. RE >> 1000
	if( NFLCHZ == 4 && (DP > PRESET) )
	{
		chz[I] = -18.0f * log10f( VARCHZ[I] / (DP*12.0f) );
	}
}
