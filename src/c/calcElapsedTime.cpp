#include "Header_CPP.h"

// Display the elapsed time
double calcElapsedTime( const struct timespec* start, const struct timespec* end )
{
	struct timespec temp;

	if ( (end->tv_nsec - start->tv_nsec) < 0 )
	{
		temp.tv_sec = end->tv_sec - start->tv_sec - 1;
		temp.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
	}
	else
	{
		temp.tv_sec = end->tv_sec - start->tv_sec;
		temp.tv_nsec = end->tv_nsec - start->tv_nsec;
	}

	double total = (double)temp.tv_sec + (double)temp.tv_nsec/1000000000;

	return total;

//	printf("\n%s", msg);
//	printf("\n\tsec:  %f\n\tmsec: %.5f\n\tusec: %.2f\n\tnsec: %.0f\n", total, total*1000, total*1000*1000, total*1000*1000*1000 );
}
