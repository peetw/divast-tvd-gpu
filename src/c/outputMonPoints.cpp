#include "Header_CPP.h"

void outputMonPoints(	ostream &fileMonPoints, const size_t ARRAY_MEM_1, const int JMAX,
						const int NMONIT, const int numStep, const float DX, const float TMONIT, const float timeSim,
						const int* IMONIT, const int* JMONIT, const float* H, const float* etu,
						const float* qxu, const float* qyu, const float* tbx, const float* tby, float &timeMon)
{
	// Update output time
	timeMon += TMONIT;

	// Calculate total volume of water in domain
	float volume = 0.0;

	for( unsigned int i = 0; i < ARRAY_MEM_1; i++ )
		volume += H[i] + etu[i];

	volume = volume * DX*DX;

	// Write monitoring point data to file
	fileMonPoints.precision(4);
	fileMonPoints << std::scientific << std::uppercase << timeSim << "\t" << volume << "\t";

	for( unsigned int i = 0; i < NMONIT; i++)
	{
		unsigned int itmp = IMONIT[i] - 1;
		unsigned int jtmp = JMONIT[i] - 1;

		unsigned int indx = itmp*JMAX + jtmp;

		fileMonPoints	<< std::scientific << std::uppercase
						<< "\t" << H[indx] << "\t" << etu[indx]
						<< "\t" << qxu[indx] << "\t" << qyu[indx]
						<< "\t" << tbx[indx] << "\t" << tby[indx] << "\t";
	}

	fileMonPoints << "\n";
}
